# Manual run to build digram.
# Doesn't run in CI because GraphViz is slow and fragile.
# Also the resulting digram is for legacy services only.

set -e

# Faily quickly if it broke.
hugo

# Drawing charts takes a minute or two.
python3 format.py --graph --graph_per_product

# Run this after the graph files are in place.
hugo
