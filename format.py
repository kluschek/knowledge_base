import yaml
import markdown
from io import StringIO
from graphviz import Digraph
import os
import html
from pathlib import Path
import re
import sys
import argparse

# Needed because PDF will want to create absolute URL link.
BASE_URL = "https://crossref.gitlab.io/knowledge_base/docs/services/%s"



def template_simple(area, entry):
    nodename = entry['title']
    description = entry.get('desc') or ""

    return "<<font point-size='10'>%s</font><br/><font point-size='20'>%s</font><br/>%s>" % (
       html.escape(area),
       html.escape(nodename),
       splitlines(html.escape(description), "<br />"))

def template_full(area, entry):
    return """<<table cellborder="0" border="0">
                <tr><td colspan="2"><font point-size='10'>{area}</font></td></tr>
                <tr><td colspan="2"><font point-size='20'>{title}</font></td></tr>
                <tr><td colspan="2">{description}</td></tr>
                <tr><td>Repo:</td><td>{repo}</td></tr>
                <tr><td>Internal docs:</td><td>{internal_docs}</td></tr>
                <tr><td>Public docs:</td><td>{docs}</td></tr>
              </table>>""".format(
        area = html.escape(area),
        title = html.escape(entry.get('title') or ""),
        description = splitlines(html.escape(entry.get('desc') or ""), "<br />"),
        repo = html.escape(entry.get('repo') or ""),
        internal_docs = html.escape(entry.get('internal_docs') or ""),
        # Take the first entry, if available
        docs = html.escape((entry.get('docs') or [""])[0]),
    )

def area_color(area_name):
    """
    Generate a pastel coloured background colour for the area name.
    Use name hash so we get a consistent colour each run.
    """
    # Lowest value to get into 'pastel' territory
    offset = 150

    max = 256
    hash = area_name.__hash__()
    range = max - offset
    r = (int(hash / 7) % range) + offset
    g = (int(hash / 29) % range) + offset
    b = (int(hash / 53) % range) + offset

    # Hex colour in HTML format.
    return "#%02X%02X%02X" % (r, g, b)

words_per_line = 10
def splitlines(string, delimiter):
    words = string.split(" ")
    chunks = (words[i:i + words_per_line] for i in range(0, len(words), words_per_line))
    return delimiter.join((" ".join(chunk) for chunk in chunks))

def extract_frontmatter(filename):
    """Read file, extract frontmatter as a map"""
    with open(filename, "r", encoding="utf8") as f:
        content = f.read()

        # Now extract the front matter from the Markdown file.
        chunks = re.split(r"---", content, re.MULTILINE)

        # None on empty frontmatter
        if len(chunks) >= 2:
            frontmatter = chunks[1]
            return yaml.load(frontmatter, Loader=yaml.Loader)

def write_frontmatter(filename, data):
    """Replace frontmatter with supplied data."""

    with open(filename, "r", encoding="utf8") as f:
        content = f.read()
        chunks = re.split(r"---", content, re.MULTILINE)

    if len(chunks) == 0:
        print("No chunks in ", filename)
        return

    with open(filename, "w", encoding="utf8") as f:
        f.write("---\n")
        yaml.dump(data, f, Dumper=yaml.Dumper)
        f.write("---")
        f.write("---".join(chunks[2:]))


def patch_frontmatter(filename, f):
    """Given a markdown file, apply a function to its frontmatter and save."""
    frontmatter = extract_frontmatter(filename) or {}
    patched = f(filename, frontmatter)
    write_frontmatter(filename, patched)

def is_service_about_page(filename):
    # Only those files at the area/service level
    # should be e.g.
    # ('content', 'docs', 'services', 'co-acess', '_index.md')

    # Must be in the right directory.
    if filename.parts[:3] != ('content', 'docs', 'services'):
        return False

    # Must be an about file.
    if filename.parts[-1] != "_index.md":
        return False

    # Must be only specified depth.

    if len(filename.parts) != 5:
        return False

    return True

def is_topic_page(filename):
    return len(filename.parts) >= 3 and filename.parts[:3] == ('content', 'docs', 'topics')

def is_component_page(filename):
    return filename.parts[:2] == ('content', 'components') and filename.parts[-1].endswith(".md")

def is_product_page(filename):
    return filename.parts[:2] == ('content', 'products') and filename.parts[-1].endswith(".md")

class Pages:
    def __init__(self, base_dir):
        self.base_dir = base_dir

        # service id -> metadata
        self.services = {}

        # taxonomy type -> union of all values.
        self.taxonomy_values = {}

        # Relevant for this graph are 'services' which are denoted by
        # <service>/_index.md in the hugo content structure.
        for filename in Path(base_dir).rglob('_index.md'):

            # This only applies to the about pages of services.
            if not is_service_about_page(filename):
                continue

            # ID of the page is area/service which is deduced from the
            service = filename.parts[3]

            frontmatter = extract_frontmatter(filename)

            # Skip pages with empty front matter.
            if frontmatter is None:
                continue

            self.services[service] = frontmatter

            # Collect all types that have list keys, whatever they are.
            for (k, v) in frontmatter.items():
                if isinstance(v, list):
                    items = set(v)
                    prev = self.taxonomy_values.get(k, set())
                    self.taxonomy_values[k] = prev.union(items)

    def check_services(self):
        "Check service dependencies all exist."

        known_services = set()
        referred_services = set()

        for (id, entry) in self.services.items():
            known_services.add(id)
            # We may return None, hence using `or` rather than default value.
            deps = (entry.get('servicedeps') or []) + (entry.get('datadeps') or [])
            for s in deps :
                if s == id:
                    print("Error! Self-reference in: %s" % id)
                if s:
                    # Badly formatted YAML may result in
                    # list of single characters.
                    if len(s) == 1:
                        print("Error in: %s" % id)

                    referred_services.add(s)


        unknown = referred_services - known_services
        if unknown:
            print("ERROR! Services referred to but unknown:")
            for x in unknown:
                print(x)
            exit(1)



    def graph_nodes(self, filename, format, template_f, color_f=None, rankdir='LR'):
        "Graph with no areas deliniated."
        g = Digraph(comment='Crossref Services', format=format, engine="dot")
        g.attr(rankdir=rankdir)
        g.attr("node", shape="box")
        g.attr("node", style="rounded")
        # This is necessary to read the diagram but crashes graphviz on GitLab
        # (but not locally!)
        g.attr(splines="ortho")

        # Pen width.
        USER_FACING = "2"
        NOT_USER_FACING = "1"

        with g.subgraph(name="cluster_legend") as c:
            c.attr(label = "Legend")
            c.node("legend_data_from", "")
            c.node("legend_data_to", "")
            c.node("legend_dep_from", "")
            c.node("legend_dep_to", "")
            c.edge("legend_dep_from", "legend_data_to", "Depends on", color="darkolivegreen", penwidth="2")
            c.edge("legend_data_from", "legend_dep_to", "Data flows to", color="dodgerblue3", penwidth="2")
            c.node("legend_user_facing", "User Facing", peripheries=USER_FACING)
            c.node("legend_not_user_facing", "Not user facing", peripheries=NOT_USER_FACING)

        for (id, entry) in self.services.items():
            if 'area' not in entry:
                print("Error: no 'area' in ", id)
            area = entry['area']
            name = id

            # Hyperlink to where the page is rendered.
            href = BASE_URL % id
            entry['internal_docs'] = href

            # If there's no colouring function, base it on the area name.
            if color_f:
                fillcolor = color_f(entry)
            else:
                fillcolor = area_color(area)

            penwidth = USER_FACING if entry.get('userfacing', False) else NOT_USER_FACING
            g.node(id,
                   template_f(area, entry),
                   fillcolor=fillcolor,
                   style="filled",
                   peripheries=penwidth,
                   href=href)

            # Service deps
            deps = entry.get('servicedeps') or []
            for dep in deps:
                # Draw arrow backward but keep direction forwards so that layout
                # is left to right.
                g.edge(dep, id, color="darkolivegreen", penwidth="2", dir="back")

            # Data flow deps.
            deps = entry.get('datadeps') or []
            for dep in deps:
                g.edge(dep, id, color="dodgerblue3", penwidth="2")

        g.render(filename)

def lint_index(filename, frontmatter):
    # Replace null with empty dictionary for consistency.
    # We expect nothing for most _index pages as they are mostly just for structure.
    if frontmatter is None:
        return {"title": ""}
    else:
        return frontmatter

DEFAULT_ABOUT_DICT = {
    'prod_urls': [],
    'prod_heartbeats': [],
    'staging_urls': [],
    'staging_heartbeats': [],
    'products': [],
    'datadeps': [],
    'servicedeps': [],
    'tags': [],
    'packages': [],
    'related_services': [],
    'repo_links' : [],
    'title': '',
    'lang': '',
    'lead': '',
    'owner': '',
    'desc': '',
    # Need to supply an area for all services, though it's only for legacy.
    # 'Components' are the new categorisation system.
    # So new services use the 'greenfield' area.
    'area': 'greenfield',
    'components': '',
    'docs': [],
    'userfacing': False,
    'sentry_url': '',
    'sonar_url': '',
    'legacy': True,
}

DEFAULT_COMPONENT_PAGE_DICT = {
    'doc_link': [],
    'label_name': None,
    'title': '',
    'weight': 0
}

DEFAULT_OTHER_PAGE_DICT = {
    'tags': [],
    'title': '',
    'related_services': [],
    'repo_links' : [],
    'weight': 0
}

DEFAULT_TOPIC_PAGE_DICT = {
    'products': [],
    'desc': '',
    'tags': [],
    'title': '',
    'related_services': [],
    'repo_links' : [],
    'weight': 0
}

DEFAULT_PRODUCT_PAGE_DICT = {
    'title': '',
    'weight': 0,
    'owner': '',
    'docs': [],
    'prod_urls': [],
}

def lint_service_about(filename, frontmatter):
    # Easy typo in keys.
    if "service_deps" in frontmatter:
        frontmatter['servicedeps'] = frontmatter.pop('service_deps')

    if "data_deps" in frontmatter:
        frontmatter['datadeps'] = frontmatter.pop('data_deps')

    keys = frontmatter.keys()
    for key in keys:

        # Empty entries are treated as strings by Hugo, so work on that basis.
        got = frontmatter[key]

        # Empty lists are falsy, we need to check explicitly for None.
        if got is None:
            got = ""

        if key not in DEFAULT_ABOUT_DICT:
            print("Unexpected key:", key)
        else:
            expected = DEFAULT_ABOUT_DICT[key]

            if key not in DEFAULT_ABOUT_DICT:
                print("Unexpected key", key, "in", filename)

            got_type = type(got)
            expected_type = type(expected)

            if expected_type != got_type:
                print("Unexpected type", got_type, "for '", key, "' expected", expected_type, "in", filename, ". Value '", got, "', expected '", expected, "'")

    with_defaults = {**DEFAULT_ABOUT_DICT, **frontmatter}

    # Because the YAML parser defaults to None for empty entries, patch the default ones in if they are empty.
    # This could be e.g. empty list, empty string etc.
    for key in with_defaults.keys():
        if with_defaults[key] is None:
            with_defaults[key] = DEFAULT_ABOUT_DICT[key]


    return with_defaults

def lint_other_page(filename, frontmatter):
    keys = frontmatter.keys()
    for key in keys:
        if key not in DEFAULT_OTHER_PAGE_DICT:
            print("Unexpected key", key, "in", filename)

    with_defaults = {**DEFAULT_OTHER_PAGE_DICT, **frontmatter}

    return with_defaults

def lint_topic(filename, frontmatter):
    keys = frontmatter.keys()
    for key in keys:
        if key not in DEFAULT_TOPIC_PAGE_DICT:
            print("Unexpected key", key, "in", filename)

    with_defaults = {**DEFAULT_TOPIC_PAGE_DICT, **frontmatter}

    return with_defaults

def lint_component(filename, frontmatter):
    keys = frontmatter.keys()
    for key in keys:
        if key not in DEFAULT_COMPONENT_PAGE_DICT:
            print("Unexpected key", key, "in", filename)

    with_defaults = {**DEFAULT_COMPONENT_PAGE_DICT, **frontmatter}

    return with_defaults

def lint_product(filename, frontmatter):
    keys = frontmatter.keys()
    for key in keys:
        if key not in DEFAULT_PRODUCT_PAGE_DICT:
            print("Unexpected key", key, "in", filename)

    with_defaults = {**DEFAULT_PRODUCT_PAGE_DICT, **frontmatter}

    return with_defaults

def lint_pages(base_dir):
    """
    Lint all pages (not only services).
    Different page types have different needs.
    NB: Even the fact of loading and then writing the YAML will regularize it.
    """
    for filename in Path(base_dir).rglob('*.md'):

        # service about pages have special set of frontmatter items
        if is_service_about_page(filename):
            patch_frontmatter(filename, lint_service_about)

        elif is_topic_page(filename):
            patch_frontmatter(filename, lint_topic)

        elif is_component_page(filename):
            patch_frontmatter(filename, lint_component)

        elif is_product_page(filename):
            patch_frontmatter(filename, lint_product)

        # otherwise the standard set of fields.
        else:
            patch_frontmatter(filename, lint_other_page)




print("Init...")

# Build from Hugo structure.
BASE_DIR = "content"
s = Pages(BASE_DIR)
s.check_services()

os.makedirs("content/rendered", exist_ok=True)

# Write into a place in the hugo content structure which is .gitignored .
# Sometimes the orthogonal layout crashes, which can be fixed by changing the rankdir.
# This makes it harder to read, but it's better than nothing.


###

parser = argparse.ArgumentParser()

parser.add_argument("--lint", action="store_true", help="Lint all pages in place.")
parser.add_argument("--graph_per_product", action="store_true", help="Produce graph per product for Hugo site.")
parser.add_argument("--graph", action="store_true", help="Produce main graph for Hugo site.")
args = parser.parse_args()


if args.lint:
    print("Linting...")
    lint_pages(BASE_DIR)

if args.graph:
    print("Generating PDF...")
    try:
        s.graph_nodes("content/rendered/dependencies", "pdf", template_simple, rankdir="LR")
    except:
        s.graph_nodes("content/rendered/dependencies", "pdf", template_simple, rankdir="TB")

    try:
        s.graph_nodes("content/rendered/dependencies", "svg", template_simple, rankdir="LR")
    except:
        s.graph_nodes("content/rendered/dependencies", "svg", template_simple, rankdir="TB")

    try:
        s.graph_nodes("content/rendered/dependencies_full", "pdf", template_full, rankdir="LR")
    except:
        s.graph_nodes("content/rendered/dependencies_full", "pdf", template_full, rankdir="TB")

    try:
        s.graph_nodes("content/rendered/dependencies_full", "svg", template_full, rankdir="LR")
    except:
        s.graph_nodes("content/rendered/dependencies_full", "svg", template_full, rankdir="TB")

if args.graph_per_product:
    products = s.taxonomy_values['products']
    for product in products:
        print("Generating Product graph: ", product)
        def color_f(info):
            #print(info)
            if product in info.get('products', []):
                return "#b0b0b0"
            else:
                return "#fafafa"

        path = "content/rendered/" + product
        try:
            s.graph_nodes(path, "svg", template_simple, color_f=color_f, rankdir="LR")
        except:
            s.graph_nodes(path, "svg", template_simple, color_f=color_f, rankdir="TB")
