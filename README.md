This is the Knowledge Base. It is an internal documentation site for use by Crossref staff, but it's open.

The site is built using Hugo, but there's some magic to create a diagram of how all of our services connect.

To get started head to <https://crossref.gitlab.io/knowledge_base/>

This is a work in progress and does not fully represent the current state of Crossref's systems.