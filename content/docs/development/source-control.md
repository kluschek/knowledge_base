---
related_services: []
repo_links: []
tags: []
title: Source Control
weight: 2
---

## Branching Model & Workflow

We use a very simple branching model, with `develop` for active development, feature branches for implementation work and `master` for tagged release-ready. It's simple enough not to need additional tools beyond `git`.

{{< mermaid >}}
sequenceDiagram
    develop->>feature/«NAME»: Create feature branch.
    feature/«NAME»->>feature/«NAME»: Commit mentioning issue.
    feature/«NAME»->>develop: Merge request and review
    develop->>master: Release via merge request.
    master->>tag «vX.Y.Z»: Tag release for deployment
    tag «vX.Y.Z»->>tag «vX.Y.Z»: Auto build of release output.
{{< /mermaid >}} 

1. The `develop` branch is the long-running branch.
1. The `master` branch is used to tag releases. By keeping these separate we can make emergency hotfixes to master without merging in develop.
1. Code changes should be made in feature branches, branched off `develop`. The name of the branch should start `feature/` and have a relevant name.
1. Once the feature branch has been merged into `develop` it should be deleted. We should have no long-running branches except `master` and `develop`. 
1. Before starting work make sure you've pulled the latest `develop`.
1. Don't be afraid to make small changes to your feature branch and push them. The merge will usually be squashed, so the diff will show as one single change. 
1. Look at the CI results as you push feature branches. This will avoid a nasty shock later on.
1. Prefix your merge request title with 'WIP:' if it's not ready for review. Remove this prefix when it's ready.
1. Merge Request should be against the `develop` branch.

### Step by step 

1. Ensure the change you want to make is represented by one or more specific issues. That could be a User Story, e.g. `user_stories#5` or an issue on the same repo as the code, e.g. `#5`.
1. Check out the `develop` branch and pull to ensure you're up to date.
   - `git checkout develop`
   - `git pull origin develop`
1. Create a feature branch off develop for your issue:
   - `git checkout -b feature/add-widgets`
   - `git branch` should tell you you're on `feature/add-widgets`.
1. Make changes to the code. Commit as often as you want to your branch.
   - `git add myfile.xyz`
   - `git add src/java/my/package`
   - `git commit -m "Add widgets for issue #5"`
1. Push as often as you want to your feature branch.
   - `git push origin feature/add-widgets`
1. Create a Merge Request, asking that your feature branch is merged back to `develop` (not `master`). When you push a branch GitLab will construct a ready-made URL for you which appears in the console. Or do it manually via GitLab interface.
1. Assign the Merge Request to the reviewer.
1. Reviewer will review, give feedback. You may need to make more commits to your branch and push them in response to the review.
1. When the review is concluded, it will be automatically merged back to develop.

We may also welcome Merge Requests from developers in the community.


## Merging

1. Follow [Code Review process]({{< relref "quality-defects/#code-review" >}}). Don't merge if it doesn't pass.
1. Merges should usually be from `feature/«name»` to `develop`. If needed, the feature branch can be rebased against `develop` via the GitLab interface. 
1. Tick the 'squash merge' box so that the whole feature branch can be merged in one go. Take the commit message from the description of the Merge Request. Ensure that relevant issue numbers are included in the message. 
1. Once merged, go to the issue, review the acceptance criteria on the relevant ticket(s) per the Code Review process.

## Releasing

To create a release:

1. Observe the latest tag. Use [Semantic Versioning](https://semver.org/) to determine the new version number, e.g. `v0.1.1`.
1. Write release notes for the version. Include a headline that starts with the version number and a summary. Underneath mention the changes, with links to issues. 
1. Create a Merge Request from `develop` to `master` with a merge describing the new version, including the version number. Wait for the CI to give the all-clear, then merge across. Don't squash merges.
1. Once merged, create the tag on master with the release.
1. Wait for CI to build and package the Docker image into the repo's Docker Registry.

## Continuous Integration

1. CI is used for:
     - Compilation checks
     - Test run
     - Compilation and packaging (into WAR or Docker image)
     - Static Analysis
1. Most of this should run for all branches.
1. Test reports should be available on the CI Artifacts page.
1. Static Analysis in SONAR is triggered by CI, but you must go and look deliberately at the SONAR report to see the results. 