---
related_services: []
repo_links: []
tags: []
title: Languages and Codebases
weight: 1
---

## Languages

1. We use Java, Clojure, JavaScript, PHP, Perl and some Python and Ruby.
1. We will write new code in Java, Clojure, JavaScript, Python.
1. We should try to use the same or equivalent tools and approaches across languages. Not everything translates directly.

## Codebases

1. Each codebase is defined as Legacy or Greenfield. 
1. We can't fix Legacy codebases but we can gradually improve them.
1. All Greenfield projects will meet the standards described in this document from the start.
1. Greenfield code bases will be open source unless there's a specific reason. Don't put anything there that you don't want to be public, whether it's sensitive data or words.
1. Each new codebase should follow the template for the language.
   1. [Java / Spring Boot starter project](https://gitlab.com/crossref/java_starter_project)
   1. [Python / Django starter project](https://gitlab.com/crossref/django_starter_project/)
1. Each project should have CI builds for test runs, running tests and packaging and building in Docker. If you follow the 'starter project' this should be set up.

