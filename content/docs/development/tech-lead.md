---
related_services: []
repo_links: []
tags: []
title: Technical Leadership
weight: 0
---

## Ownership

We are working toward a modular Crossref system made up of [components]({{< relref "/components" >}}) and constituent services. All greenfield development will fit this pattern. Our legacy services don't quite fit this structure, and we will be gradually rewriting and adapting them to fit the new model.

Each component will have a Tech Lead and Product Owner respectively. A component is implemented by one or more services. Technical and product ownership covers all services in a component. 

A given Product or Project will usually implicate a number of components. For example, the Crossmark product involves ingestion, schema, indexing, matching and so on. Therefore each product will have a distinct product owner and technical lead. These may or may not overlap with the owners of the components involved.

## Legacy Exceptions

For legacy code, where some concerns are mixed, it may be necessary to blur the lines. Not all legacy services will cleanly fit into a single component. In these cases we will treat the service as belonging to the most relevant component and assign technical or product leadership on a case by case basis. We will do this in response to support tickets or feature requests, but not as a blanket exercise.

## Responsibilities

The tech lead for a given component, service or product has these responsibilities. Much of this can be shared with the head of software development — it doesn't have to be a massive burden. And for legacy services we will need to be flexible.

### Architecture and technology

 - Consult with the head of software development on changes that will affect the architecture of the component or wider Crossref system.
 - Consult with the head of software development on new technology choices and regular reviews.

### Infrastructure

 - Partner with the infrastructure lead for the service to plan any required changes in infrastructure.

### Features and specification

 - Refine bugs and features with all relevant parties. This can be done asynchronously: no need to wait for meetings.
 - Collaborate with the Product Owner to specify user stories and issues. These must be sufficiently detailed to implement with satisfactory code coverage and regression testing. This may involve working with the owner to produce test cases.
- As part of a project roadmap, refine new user stories to the agreed definition of ready prior to triage. 
- In response to bugs, refine new issues so that they are ready to be worked on prior to triage.
- Propose API designs for features, validate them as part of the specification process.
- Coordinate with head of software development for cohesive overall API design.
- Identify product metrics in new features.
- Provide product owners with the data to make product decisions.

### Operations

- Respond to, and investigate errors. File and refine issues to bring to triage.

### Development

- Spec tickets such that any developer could theoretically do the work without extra information.
- Where work is delegated to another developer, provide any necessary help.
- Be responsible for the git repositories for all services in the component.
- Ensure each repository is set up correctly (branches, continuous integration, test coverage and code quality checks).
- Review and merge code to ensure that it meets quality criteria. The tech lead doesn't have to review all code in their component. But they should write / refine sufficiently complete issues so that a reviewer can assess the code.

### Planning

- Where work is part of a larger feature, provide an implementation plan. Then work with the product owner, head of software development, and infrastructure lead to refine the plan. 
- Plan releases for services, write release notes, tag and monitor deployments.

### Out of scope

Tech leads may get involved, but are not accountable for:

- Outreach planning
- Infrastructure planning

