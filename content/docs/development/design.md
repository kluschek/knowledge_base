---
related_services: []
repo_links: []
tags: []
title: Software Design
weight: 3
---

## Design of Services

1. We follow most of the guidelines in [Java For Small Teams](https://ncrcoe.gitbooks.io/java-for-small-teams/content/).
1. Aim for good test coverage.
   1. Though it's standard, we don't (yet) put a minimum number on code coverage.
   1. If it's cumbersome to test something, maybe it's time for a refactor.
   1. IntelliJ has great tooling for running tests!
1. One service is exposed at one hostname / subdomain. (We currently break this convention with the REST API, where paths require routing. We also break it with the apps.crossref.org subdomain. )  
1. We use [Spring Boot](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/html/) for new Java apps. We'll follow Spring's defaults. 

## 12 Factor

1. We will follow [12 Factor App](https://12factor.net/).
1. Greenfield services should build into a container. The container should be pushed into the repo's repo by CI. This, along with automatic builds, will be automatic if you start with a starter project.
1. Configuration should be done with environment variables. These should be listed in the README.
1. No configuration should be included in the code. It should be passed in by environment variables.
1. No secrets or passwords should be included anywhere in source control.