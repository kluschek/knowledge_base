---
related_services: []
repo_links: []
tags: []
title: Documenting our Software Systems
weight: 5
---

## Documentation

1. Each Service should have an entry in the Knowledge Base.
1. The Knowledge Base is all open to the public. No secrets.
1. You can make small edits in the Knowledge Base via GitLab (the edit button at the bottom of the page). For larger edits clone the repo.
1. The repo uses only a `master` branch for simplicity. This is a trade-off. 
1. The website is build using [Hugo](https://gohugo.io/). Install it if you want to edit the site on your machine.
1. The site is automatically built with CI and published to <https://crossref.gitlab.io/knowledge_base> 
1. To create a new service:
    1. Each service should be a directory under `content/docs/services`. The name of the directory (e.g. `crossmark-dialog`) is the identifier used to refer to the service.
    1. `_index.md`, leave empty. This is required for the directory struture.
    1. `about.md` with a table of contents (work from another service's frontmatter as a starter). Ensure the `servicedeps` and `datadeps` lists are completed. Service Dependencies are things that this service connects to. Data Dependencies are services where data comes from. They are often subtly different.
    1. Fill out the tag list to help cross-linking.
    1. Run `hugo` at the command line to check that the links are all correct. This will verify that there are no typos in your `servicedeps` or `datadeps`.
    1. After pushing, check that your checkin didn't break the CI build. 
1. To preview locally run `hugo serve` and visit <http://localhost:1313>
1. Periodically run `python3 format.py --lint` to update pages with the necessary front-matter items and generally clear up.

