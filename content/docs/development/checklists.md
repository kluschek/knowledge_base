---
related_services: []
repo_links: []
tags: []
title: Checklists
weight: 0
---

Each service corresponds, in most cases, to a single source code repository. New repositories will usually be classified as greenfield. Most of the below assumes this. 

Many of these will be performed by the tech lead of the relevant component / service.

## New Services / Repositories

Creating a repository:

1. Put it in the Crossref group on GitLab. This is the default behaviour. We don't generally use subgroups.
1. Give it a `Title Case` name. For the slug use `snake_case`. Don't abbreviate.
1. Make it Public unless you have a specific reason not to.
1. Take a copy of the starter project, which will ensure you get the CI etc right.
     1. [Java starter project](https://gitlab.com/crossref/java_starter_project)
1. Add a README. Doing this will create the necessary "master" branch. Create "develop" branch from the "master" branch.
1. Add a License. We use MIT, see other projects.
1. Add a Continuous Integration build file `.gitlab-ci`. See [CI and Automation]({{< relref "ci-automation" >}}).
1. Edit the Project's Labels:
   - Delete the per-project labels, so we only use Crossref-wide ones.
   - Star the useful Crossref-wide ones so they show up.
1. In the General settings, copy the default issue template from the [Issues](https://gitlab.com/crossref/issues/edit) repository.
1. Review the [Knowledge Base](https://gitlab.com/crossref/knowledge_base/tree/master) and add an entry for the service:
  1. Create an empty `content/docs/services/«service»/_index.md` and run the lint script.
  1. Populate the Title, Repo URL, SONAR URL, and the production, staging and develop URLs.
  1. Collaborate with a Product Manager to ensure the right info is captured.
  1. Add the services and data flow that this new service depends on. Do the same for any services that depend on the new service or recieve data from it.
1. Add to SONAR.
1. Add to Sentry.
1. Configuration and deployment should be coordinated with infrastructure and head of software development.
1. Protect the develop and master branches per the branching strategy.
1. Set the restrictions on the repository so that issues must be mentioned in to all commits.
1. Ensure that the Docker image can be built in CI.


## Definition of ready

As we plan changes to code we should take into account:

 - What are the test cases? Is the product spec sufficient to write them? If not, talk to the product owner.
 - Does this change the flow of data? If so, update the knowledge base and check in with the tech lead of that service.
 - Does this result in a change to the behaviour of the system? If so, ensure that the product owner has the right details to write up in the public education or other docs.

## Definition of done
Code changes should not be merged unless:

 - Changes in any documentation are accurate.
 - Quality criteria are met in SONAR.
 - Any database migrations identified and working?
 - Some time spent with the product owner to demonstrate the feature. Criteria for the change should be specified in advance so it's clear to the developer how to meet it.
 - Definition of done should include either tagging a release the creation of a ticket to do this. 

## Merging / code review
 - Is there a merge request for the knowledge base coupled to this?
 - Did it result in a CI deployment? Have you monitored to make sure it worked correctly?

## Planning releases
 - In most cases each feature branch will result in a change in the version number and a deployment. 
 - Release notes should be based around the feature issue or issues that contributed to the release. This will have been agreed with the product owner in advance.
 - Like all greenfield development artifacts, release notes will be public. Some of our users are interested in digging through our code and releases, so don't assume too much internal knowledge to interpret. 
 - A release will be tagged on the master branch of the project. Work should be planned and sequenced so that it's merged to the develop branch, then to master, in a releasable order. Making a number of smaller releases, and regularly rebasing feature branches, keeps this flexible.
 - Where necessary, prior to a release, invite the product owner to smoke test.
 - If the change involves database migrations ensure that they succeed and the system still works as expected.
 - The deployment will be triggered with CI automatically from the tag. Perform adequate supervision.

## Performing a release of CS

A new release is made by merging `develop` to `master`.

1. Go to the project and find the latest deployed tag version number. You'll refer to it later.
2. Create a new merge request from `develop` to `master`.
   1. Go to the Project page.
   2. Click 'Merge Reqests' -> 'New Merge Request'
3. Select `develop` as the source branch and `master` as the target branch.
4. Scroll to the bottom of the merge request and review the diffs. 
   1. The 'Changes' tab shows diffs are being introduced.
   2. The 'Commits' tab shows issues addressed. Commits should link to issue numbers.
5. Decide on a new version number. 
   - It should be of the format `vMAJOR.MINOR.PATCH`.
   - We use [Semantic Versioning](https://semver.org/). This isn't suitable for all of our projects but it's beneficial to use the same scheme across our codebases. Most of the time we will be making a backwards-compatible change, so the MAJOR number (middle) will be incremented.
   - You will normally increment the middle number, e.g. `v0.100.4` would become `v0.101.0`.
6. Scroll to the top and name your merge request after the version number, e.g. "v0.101.0".
7. Write a one-line description of this release in the description.
8. Write a bullet-pointed list of the changes that have gone into the release.
   - Reference issues with the correct syntax e.g. `crossref/user_stories#107` so they can be linked back.  
   - Browse back at previous releases for inspiration. There should be continuity of content and style.
9. The notes should allow:
   - Users outside Crossref to understand what changes they might expect from release.
   - Support to identify changes that could explain a change in behaviour.
   - Product managers to know at which point a given user story or fix was released.
   - Support to know which features went into production at a given point in time.
10. Save the merge request.
11. One final proof-read, feel free to ask a colleague to review although it's not required as all changes should have been code-reviewed before merging to develop.
12. Wait for the build and test pipeline to go green. If it doesn't, stop and review.
13. Click 'merge'. Do not 'squash' commits (each merge request to **into** develop could have squashed)
14. Now master is up to date, create a tag.
   1. Note the latest commit hash that was merged to master.
   2. Copy the text of the merge request for later.
   3. Click 'repository' -> 'tags'.
   4. Give your tag the same title.
   5. Select "Create from `master`"
   6. Copy the description into the Release Notes field.
15. Double-check that the tag represents the correct commit hash. In the past there has been a race condition where the latest version of the branch didn't end up being comitted.
16. The compiled output (e.g. WAR file) will be built by CI with the appropriate version number. 
