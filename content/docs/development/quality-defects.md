---
related_services: []
repo_links: []
tags: []
title: Quality and Defects
weight: 4
---

## Bugs

1. Errors are reported via [Sentry](https://sentry.io/). It has a plugin for [Spring Boot](https://sentry.io/for/spring/).
1. We have weekly triage meetings to address each issue in Sentry.
1. Once we get through the backlog of errors we'll change our workflow to a more proactive one.
1. We'll review this process as we go.

## Error Triage 

The Error Triage meeting will follow:

1. Filter Sentry by 'recent'.
1. Each entry, zoom out to 90 days to see context.
1. Create GitLab issue and link it to Sentry. [Create Issue »](https://gitlab.com/crossref/issues/issues/)
1. The issue should take the form:
   -  **Malfunction.** → GitLab 'bug' Issue, snooze for 1 week. 
   -  **Bug in the code.** → GitLab 'bug' Issue, snooze for 1 week
   -  **Application Notification.** → This is likely a feature that should be raised to the Product team to be removed from Sentry workflow. Either the message should be included in a report for Product, or sent to the user, or instrumented some other way. Issue should mention that we remove the error message from the codebase. 
1. Check for confidential details before pasting.
1. Link the issue to Sentry.
1. Snooze the error in Sentry for a period, e.g. 6 months. We will use the GitLab issue to track.
1. The Product Triage team will pick up and prioritise the issue. High priority issues should be marked as such. 

## Legacy to Greenfield

1. We will gradually improve our Legacy codebase, but we won't aim to make any fundamental changes.
1. As we focus on services, we will extract the code or rewrite the service wholesale.
1. If the code is simply extracted it will be classed as Legacy, but we will be in a better position to improve it. We will have to be tolerant of service boundaries being a little inexact.
1. If it is re-written it will be classed as Greenfield.


### Static Analysis

1. We use SONAR for Java and Python. 
1. We use [eastwood](https://github.com/jonase/eastwood) and [clj-kondo](https://github.com/borkdude/clj-kondo) for Clojure.
1. We will follow the rules to the letter for Greenfield projects. Sometimes they are cumbersome, but the benefit of a clean codebase nearly always outweighs the annoyance. 
1. IntelliJ has an excellent plugin which will flag things before they are picked up with CI and get quick feedback.


### Code Review

These should be met before submitting for code review. Reviewer should also confirm as the first step of code review.

1. Code should *not* be merged if:
   1. Tests don't pass. 
   1. For Legacy codebases, SONAR results should not have got _worse_.
   1. For Greenfield codebases, SONAR results should be green.
   1. They are not linked to an issue number(s) in commit messages. Click it to make sure it works.
1. Review requirements in the linked issues / user stories. Make sure all functionality is represented by tests and implemented. 
1. If code needs fixing, make the change and resubmit:
   1. If the merge reduces the test coverage, remove unused code or refactor to write new tests.
   1. If there is behaviour that hasn't been specified to the point where a unit test can be written, work with the Product Owner to clarify requirements.
1. Check the 'code review' box on the relevant tickets. Review other developer actions on the ticket. Don't move to 'done' until all boxes are ticked.

### Style

We use standard style guides. Reformat your code before committing. 

#### Java

1. We use Google Java Style for Java. You can [download it here](https://raw.githubusercontent.com/google/styleguide/gh-pages/intellij-java-google-style.xml) and import into Intellij.
1. Reformat your project before committing. In Intellij you can right-click on the project tree and 'reformat code'.

#### Python

1. PEP8 is the official Python style.
1. Google's [YAPF](https://github.com/google/yapf/) tool can be used to reformat files. Use `yapf -i -r /path/to/code`.

#### Clojure

1. We use [Clojure Style Guide](https://guide.clojure.style).
