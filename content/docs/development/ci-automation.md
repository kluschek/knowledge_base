---
related_services: []
repo_links: []
tags: []
title: CI, Automation, Deployment and Environments
weight: 0
---

Continuous Integration, Automation and Deployment are all closely related. 

The CI script for a given repository will:

1. Run tests.
1. Check and gate quality.
1. Build the code into a deliverable package, usually a Docker image, and distributing it.
1. Trigger deployments to `development`, `staging` and `production`.

This must be configured for every repository, but most will follow the same pattern.

## Running tests

CI should trigger unit tests on every commit to any branch:

  - On feature branches to demonstrate that each is is ready to merge to `develop`.
  - On every merge to the `develop` branch to demonstrate that all merged feature branches continue to function.
  - On every merge to `master` so that we know that a tagged deployment is ready to ship.

The CI environment should spin up any needed services to run tests. Any secrets, even for transient containers, should be stored in the repo's secret storage, not in the CI file.


## Checking and gating quality

The build should break when the branch isn't in a deployable state. This will prevent it from being merged or deployed until it's ready.

 - Any failed tests should break the build.
 - Code coverage should be exported from test runs and included in the SONAR report.
 - SONAR should be triggered for feature, develop and master branches.
 - If the SONAR quality gate is not met (static analysis of code coverage) the build should break.  

## Building and distributing images

 - Images should be built from the CI pipeline for the `develop` and `master` branches, and for tags.
 - Images should be pushed into the GitLab Docker repository for easy retrieval by developers.
 - Images should also be pushed into the AWS Docker repository for deployment in AWS.

## Triggering deployments

There are three AWS environments: 

  - `develop`, automatically deployed from the `develop` branch.
  - `staging`, automatically deployed from the `master` branch.
  - `production`, automatically deployed on every tag on the `master`branch.

They contain three parallel instances of the Crossref AWS system, with the same services and infrastructure. These are defined in separate Terraform files which span across services. CI automation deployment will deploy to each of these environments.  

As part of the work of setting up a new [ECS Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs_services.html), that service's ECS Service Definition will have been created. The CI pipeline for a service will include a deployment step which will update the [ECS Task definition](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definitions.html) and kick off the new version of the service.



 