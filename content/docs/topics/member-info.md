---
desc: The systems and processes involved in Crossref member information
products: []
related_services:
- member-info
repo_links: []
tags:
- member-info
title: Member Info
weight: 0
---

This subject is big and complicated, so this page is a work in progress. Member Info is currently under review and refactoring into a separate microservice.


