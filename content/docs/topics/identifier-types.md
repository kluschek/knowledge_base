---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- doi
- orcid
- ctn
- contributor
- twitter
- updates
- prefix
- isbn
- issn
- funder-registry
- members
- shortdoi
title: Identifier types represented in Crossref.
weight: 0
---



Identifiers are used in (at least) the following places:

 - XML Schema
 - Event Data
 - REST API
 
Identifiers for non-resolvable things, such as ISBNs, are represented as URIs to make them more widely compatible with software that consumes the API data. For example, it enables ISBNs to be represented in RDF.  In some cases the identifier is a string (such as ISBN) which makes it easy to represent. In other cases the item is not so simple, e.g. a free-form contributor, and it must be transformed to make it fit in a URI.
 
Some identifiers are URLs or URIs in the first plae (such as ORCIDs and DOIs). Some are naturally represented as structured strings (such as ISBNs). Some are more complicated, such as contributors. Not all of these are transformed into URIs, e.g. CTNs.

## Types of identifier

### Contributor

IDs for ambiguous contributors. 

Represented in:

 - REST API
 
The REST API uses the `http://id.crossref.org/contributor/` URI prefix to represent contributors. They are semi-unique IDs, analogous to RDF's 'blank nodes'. These are not expected to be resolvable. They are constructed from free-form text and are a combination of the DOI, the position in the author list.

### Plain URL

Represented in:

 - Event Data
 
 
### Supplementary items

Represented in:

 - REST API

The REST API uses the `http://id.crossref.org/supp/` URI prefix to represent supplementary items. These are not expected to be resovlable.

### Tweet IDs

Represented in

 - Event Data
 
Twitter's terms prohibit the storing of an actual tweet URL. Therefore the ID is represented in a constructed URI which enables the tweet to be identified but needs to be deconstructed in order to be used.
 
### Clinical Trial Number (ctn)

Represented in:

 - REST API
 
These are normalized into a string for searching, but are not turned into URIs.
 
### DOI

Represented in:

 - REST API
 - XML Schema Funder Registry
 - XML Schema References
 - XML Schema Relations
 
The REST API uses the `http://dx.doi.org/` URI prefix to represent DOIs. These are expected to be resolvable. They do not yet conform to the current display guidlines.
 
### Short DOI

Represented in:

 - REST API
 
The REST API uses the `http://doi.org/` URI prefix to represent Short DOIs. These are expected to be resolvable. It's not clear whether this is actually used.

### Funder Registry DOI

Represented in:

 - REST API
 
Funder DOIs are a special case of DOI.
 
### ISBN

Represented in:

 - REST API
 
The REST API uses the `http://id.crossref.org/isbn/` prefix to represent ISBNs. These are not expected to be resolvable.
 
### ISSN

Represented in:

 - REST API

The REST API uses the `http://id.crossref.org/issn/` prefix to represent ISSNs. These are not expected to be resolvable.
 
### Crossref Member ID

Represented in:

 - REST API
 
The REST API uses the `http://id.crossref.org/member/` prefix to represent member IDs. These are not expected to be resolvable.
 
### ORCID ID

Represented in:

 - REST API
 
The REST API uses the `http://orcid.org/` URI prefix to represent ORCID ids. These are intended to be resolvable.
 
### DOI Prefix

Represented in:

 - REST API

The REST API uses the `http://id.crossref.org/prefix/` URI prefix to represent Prefixes. These are not intended to be resolvable.
 
### Work Type

Represented in:

 - REST API
 
The list of work types defined by the REST API can be retrieved at <https://api.crossref.org/v1/types>

Note that the BIBO Ontology is internally mentioned but doesn't appear to be made public.
 
### Update Type

Represented in:

 - REST API
 
The REST API defines the types of updates:

 - Addendum
 - Clarification
 - Correction
 - Corrigendum
 - Erratum
 - Expression of concern
 - New edition
 - New version
 - Partial retraction
 - Removal
 - Retraction
 - Withdrawal


### BIBO Bibliographic Ontology

The REST API represents work types using the [BIBO Ontology](http://bibliontology.com/specification.html).  See Cayene's `cayenne.formats.unixref` namespace.


