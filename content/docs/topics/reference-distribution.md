---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- ref-pref
title: Reference Distribution Policy
weight: 0
---

Reference distribution Policy is set per Owner Prefix. See the documentation on OAI-PMH, Snapshots and Cayenne for details on how it's applied (via page tags).

See: https://www.crossref.org/reference-distribution/