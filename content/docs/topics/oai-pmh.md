---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- content-types
title: OAI-PMH
weight: 0
---

## Constructing Sets 

Four "Sets" are built by `org.crossref.qs.oaipmh2.daos.BuildSetsTask`:

 - Journals
 - Book series
 - Books
 - Components
 
See `org.crossref.qs.oaipmh2.daos.OaiPmhDaoImpl` for underlying SQL queries. They use hard-coded integers that correspond to the constants defined by `BookCite`. See the Content Types page for more info on these.

### Journals Sets

With reference to `SELECT_JOURNAL_SETS` query.  Every entry in the JournalCite table that has both a Publisher ID and a Journal Title, that is not deleted. 

### Book Sets

With reference to `SELECT_BOOK_SETS` query. Every entry in the BookCite table that has both a Publisher ID and a Journal Title, that is not deleted. Type must be one of:

 - `BookCite.TYPE_BOOK`
 - `BookCite.TYPE_CONF`
 - `BookCite.TYPE_DISSERTATION`
 - `BookCite.TYPE_REPORT`
 - `BookCite.TYPE_STANDARD`
 - `BookCite.TYPE_DATABASE`

### Book Series

With reference to `SELECT_BOOK_SERIES_SETS` query. Every entry in the BookCite table that has both a Publisher ID, Journal title, that is not deleted. Type must be one of: 

 - `BookCite.TYPE_CONF_SERIES`
 - `BookCite.TYPE_BOOK_SERIES`
 - `BookCite.TYPE_REPORT_SERIES`
 - `BookCite.TYPE_STANDARD_SERIES`
 - `BookCite.TYPE_BOOK_SET`
 
### Component Series

With reference to `SELECT_COMPONENT_SETS` query. Every entry in the `CitationInfo` table that:
 
  - has a source type of `CitationInfo.DB_SRCTYPE_COMPONENT`
  - is not an alias
  - has no journalciteid
  - has no bookciteid
  - has no seriestitleid
  - has no articletitle
  - is in state 0 (?)

### Not covered

Note that `BookCite.TYPE_POSTED_CONTENT` and `BookCite.TYPE_GRANTS` are not included in OAI sets.