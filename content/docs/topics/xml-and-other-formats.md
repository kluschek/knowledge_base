---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- unixml
- unixref
- unixsd
title: XML Formats
weight: 0
---

## Types

### UNIXSD query output format

Quoting from [UNIXSD support docs](https://support.crossref.org/hc/en-us/articles/213531266-UNIXSD-query-output-format):

> UNIXSD query result includes the as-deposited publisher metadata ( as with the UNIXREF format) as well as some Crossref-generated information about the metadata record. UNIXSD is the most comprehensive metadata output format available for our metadata records.

> UNIXSD format will return deposited references if the depositing publisher has enabled reference distribution for their prefix. References will also be returned to members querying for their own deposited data.

### UNIXREF query output format

Quoting from [UNIXREF docs](https://support.crossref.org/hc/en-us/articles/214936283-UNIXREF-query-output-format)

> Our Unified XML format (UNIXREF) returns all metadata submitted by the publisher of the DOI. The UNIXREF data does not include namespaces or namespace prefixes, which are used extensively for non-bibliographic metadata.
  
> The UNIXREF format will return deposited citations if the depositing publisher has enabled reference distribution for their prefix. Citations will also be returned to members querying for their own deposited data.
  
  
### UNIXML

There are still some vestiges of UNIXML in the CS codebase.  

### Piped

Returns headers in pipe-delimited format:

    ISBN/ISSN|SER_TITLE|VOL_TITLE|AUTHOR/EDITOR|VOLUME|EDITION_NUMBER|PAGE|YEAR|COMPONENT_NUMBER|RESOURCE_TYPE|KEY|DOI

e.g. 

    0264-3561|Journal of Psychoceramics|Carberry|5|11|1|2008||| 10.5555/12345678
 
