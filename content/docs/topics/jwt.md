---
desc: ''
products: []
related_services: []
repo_links: []
tags: []
title: JWT (JSON Web Tokens)
weight: 0
---



JWT (JSON Web Tokens) is part of a broader family of technologies called JOSE (JavaScript Object Signing and Encryption). We use various parts of the family of technologies in various services.

Broadly, we use them for:

 - Authenticating authorization claims between internal actors in Event Data (e.g. Agent authorization to send Events to Percolator).
   - This gives us a trust model that allows flexibility of deployments and message bus architecture in future.
 - Authenticating authorization claims between internal actors across public infrastructure in Event Data (e.g. Percolator authorization to send Events to Bus).
   - This gives us a trust model that allows us to run a multi-cloud / multi-site setup without fiddly VPN.
 - Authenticating authorization claims from independent external actors in Event Data (e.g. Third party Agent authorization to send Events to Bus).
   - This allows us to hand out tokens to external users and ensure the provenance chain. 
 - Indicating message provenance, authenticating message provenance, and verifying integrity between independent parties in DUL (Platform sends message to Publisher, publisher verifies using signature and Key hosted by Crossref). 
   - This allows us to ensure a chain of trust between external groups of parties without having to directy intermediate.
 - Indicating the version and timestamp of the code that we distribute via CDN for Crossmark.
   - This allows us to detect when the Crossmark script has been used in an unsupported way, e.g. re-hosting, meaning bugfixes might not have been included.

See the per-service documentation for specific use-cases.

## Storage

There are two types of secrets configuration values used in Event Data. The first are JWT signing secrets, which are used to construct and validate JWTs. The second are pre-built JWT tokens, which are typically assigned to Agents and handed out to third parties (e.g. external Agents) so they can authenticate.

## Best practice and hints

Keep the JWT secrets secret. 

## Monitoring

JWT is a technology so monitoring doesn't apply. However, any service that is configured with JWTs should be monitored to detect any future errors in authentication.

## External links

 - <https://jose.readthedocs.io/en/latest/>
 - [JSON Web Token RFC](https://tools.ietf.org/html/rfc7519)

## See also

 - [JWTs in Crossmark](/crossmark/jwt)
 - [JWTs in DUL](/dul/jwt)
 - [JWTs in Event Data](/eventdata/application/jwt)

## Source code

n/a

