---
desc: ''
products: []
related_services: []
repo_links: []
tags: []
title: Messages Sent to Depositor
weight: 0
---



Crossref will send messages to the submitter of a metadata deposit. The `doi_batch/head/depositor/email_address` value is used.

## Funding Data

 - We verify the funder name (whether it is all digits or too long). We also check the consistency between provided id and name. If the checks fail, the error is logged into the message sent to the depositor.

## Validation

 - Validating the clinical trial identifier against a regular expression. The failure results in logging the error in the message sent back to the depositor.

 - If the title of an metadata element is missing or blank, an error will be sent.
 
