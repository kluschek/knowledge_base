---
desc: ''
products: []
related_services: []
repo_links: []
tags:
- content-types
title: Content Types
weight: 0
---

## Containers and Items

Content Types describe the types of content registered with Crossref. Most pieces of registered content, such as Articles or Chapters, are classified as "Items". Items generally exist in Containers. For example, an Article is a part of a Journal, a Chapter is part of a Book and a Proceeding is part of a Series. 

This does not always apply naturally to the domain model: for example Grants don't have natural containers. In these cases a virtual container type is created. This ensures that the structures are consistent across all registered content types, and can therefore be represented in the same data structures. This is mostly an implementation detail, but has implications for the design of our services.

Item metadata is generally recorded exactly as supplied by the publisher. Because Container metadata is supplied as part of every Item deposit, it is possible that two items from the same container may contradict each other in the metadata they submit for their mutual container. We therefore perform some matching of Container metadata.

## Types in the Content System

### Containers: `BookCite` and `JournalCite`

There are two tables that represent Containers. The `JournalCite` table lists Journal containers. The `BookCite` table represents Books and all other containers. The naming is an accident of history; it contains much more than books.

Because `BookCite` is polymorphic it provides the following constants to describe the type of each entry:

 - `TYPE_BOOK_OLD`
 - `TYPE_CONF_OLD`
 - `TYPE_DISSERTATION_OLD`
 - `TYPE_REPORT_OLD`
 - `TYPE_STANDARD_OLD`
 - `TYPE_DATABASE_OLD`
 - `TYPE_BOOK`
 - `TYPE_CONF`
 - `TYPE_DISSERTATION`
 - `TYPE_REPORT`
 - `TYPE_STANDARD`
 - `TYPE_DATABASE`
 - `TYPE_CONF_SERIES`
 - `TYPE_BOOK_SERIES`
 - `TYPE_REPORT_SERIES`
 - `TYPE_STANDARD_SERIES`
 - `TYPE_BOOK_SET`
 - `TYPE_POSTED_CONTENT`
 - `TYPE_GRANTS`
 
The equivalent `JournalCite`, being monomorphic, provides simply:

 - `TYPE_JOURNAL`

### Items: `CitationInfo`

A `Citation`, again through an accident of history, describes an Item, such as an article. 

Each Item belongs to a container of a given type, but also has a type and subtype of its own. Therefore every entry in the Citation Info table should relate to a row in Journal Cite or Book Cite.

`CitationInfo` defines this set of vocabulary. The correspondence between the Item type and subtype, and also that between the item type and tye conatiner type, is informally defined in code. 

Types are defined with single-character identifiers that are unique amongst Types. It also defines subtypes, one or more for each type, which are also unique within the Subtypes. There is no strict hierarchical relationship, and this does not render nicely in a diagram! 

Types:

| Constant name                | Value | Name                   |
|------------------------------|-------|------------------------|
| `DB_SRCTYPE_BOOK`            | `B`   | Book                   |
| `DB_SRCTYPE_CONF`            | `C`   | Conference             |
| `DB_SRCTYPE_COMPONENT`       | `M`   | Component              |
| `DB_SRCTYPE_NONCR_COMPONENT` | `N`   | Non-Crossref component |
| `DB_SRCTYPE_DISSERTATION`    | `D`   | Dissertation           |
| `DB_SRCTYPE_REPORT`          | `R`   | Report                 |
| `DB_SRC_TYPE_STANDARD`       | `S`   | Standard               |
| `DB_SRC_TYPE_DATABASE`       | `A`   | Database               |
| `DB_SRC_TYPE_JOURNAL`        | `J`   | Journal                |
| `DB_SRC_TYPE_POSTED`         | `P`   | Posted Content         |
| `DB_SRC_TYPE_GRANT`          | `G`   | Grants                 |

And subtypes. 

| Constant name               | Value | Name |
|-----------------------------|-------|------|
| `DB_SRCSUBTYPE_TITLE`       | `T`   | Main volume title. Used by all types. |
| `DB_SRCSUBTYPE_ISSUE`       | `I`   | Issue. Used by Journal |
| `DB_SRCSUBTYPE_VOLUME`      | `V`   | Volume. Used by Journal |
| `DB_SRCSUBTYPE_ARTICLE`     | `J`   | Article. Used by by Journal |
| `DB_SRCSUBTYPE_CONF_PAPER`  | `C`   | Conference Paper. Used by Conference |
| `DB_SRCSUBTYPE_SERIES`      | `S`   | Series. Used by Conference, Book, ReportPaper, Standard |
| `DB_SRCSUBTYPE_CONTENT`     | `C`   | Content. Used by Book, ReportPaper, Standard |
| `DB_SRCSUBTYPE_DATASET`     | `D`   | Database. Used by databases |
| `DB_SRCSUBTYPE_POSTED`      | `P`   | Posted Content. |
| `DB_SRCSUBTYPE_PEERREVIEW`  | `R`   | Peer Review |
| `DB_SRCSUBTYPE_PENDING_PUB` | `E`   | Pending Publication. |
| `DB_SRCSUBTYPE_GRANT`       | `G`   | Grant. | 
 

The type / subtype relationships are informally represented in the code that processes the relevant types' deposits. The various descendants of `AbstractMetadataElement` (one per type in the schema) are responsible for handling the associations between these. Here is the informal relationship, with `DB_SRCTYPE_` and `DB_SRCSUBTYPE_` ommitted for clarity:

 - `BOOK`
    - `TITLE`
    - `SERIES`
    - `CONTENT`
 - `CONF`
    - `TITLE`
    - `CONF_PAPER`
    - `SERIES`
 - `COMPONENT`
    - `TITLE`
 - `NONCR_COMPONENT`
    - `TITLE`
 - `DISSERTATION`
    - `TITLE`
 - `REPORT`
    - `TITLE`
    - `SERIES`
    - `CONTENT`
 - `TYPE_STANDARD`
    - `TITLE`
    - `SERIES`
    - `CONTENT`
 - `TYPE_DATABASE`
    - `TITLE`
    - `DATASET`
 - `TYPE_JOURNAL`
    - `TITLE`
    - `ISSUE`
    - `VOLUME`
    - `ARTICLE`
    - `PEERREVIEW`
 - `TYPE_POSTED`
    - `TITLE`
    - `POSTED`
 - `TYPE_GRANT`
    - `TITLE`

## Types in Cayenne REST API

The types of content are defined in Cayenne's `cayenne.ids.type` namespace. These are derived from the elements as found in the UNIXREF XML. Note that as Cayenne treats all publishing artifacts as equals, both contains and items, this list of types is the union of the above types. The precise mapping will be the subject of future documentation. 

 - `journal-article`
 - `journal-issue`
 - `journal-volume`
 - `journal`
 - `proceedings-article`
 - `proceedings`
 - `dataset`
 - `component`
 - `report`
 - `report-series`
 - `standard`
 - `standard-series`
 - `edited-book`
 - `monograph`
 - `reference-book`
 - `book`
 - `book-series`
 - `book-set`
 - `book-chapter`
 - `book-section`
 - `book-part`
 - `book-track`
 - `reference-entry`
 - `dissertation`
 - `posted-content`
 - `peer-review`
 - `other`


# Notes 

Every Citation Info row should relate to a row or rows in either Journal Cite or Book Cite tables. Every Citation Info record that contains a Journal Cite ID links to the record in Journal Cite table for that Journal Cite ID. All others have either a Book Cite ID or a Series Title ID or both in Citation Info. Those records relate to the row(s) in the Book Cite table in which the Book Cite ID matches the Book Cite ID or Series Title ID in Citation Info.

For example a book volume can be part of a book series, in which case there will be a `ci.seriestitleid = bc_bookciteid` that connects the official series title it belongs to but will also have a `ci.bookciteid = bc.bookciteid` that connects to the volume title in BC.

Not all data types necessarily have this full hierarchy, but since all but journals are handled this way, they all can use it (depending on the schema).

