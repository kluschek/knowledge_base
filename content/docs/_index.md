---
related_services: []
repo_links: []
tags: []
title: Crossref Knowledge Base
weight: 0
---

Welcome to the Crossref Knowledge Base! This site is <strong>internal documentation</strong> for use by staff. It's work in progress. If you don't work at Crossref, you probably want the [Crossref Help](https://www.crossref.org/help/) site.

## How it fits together
 - [List of Components]({{< relref "/components" >}}) (the structure we will follow for new developments)
 - [List of Products]({{< relref "/products" >}})

## Track down code
 - [List of Source Code Repos]({{< relref "/repo_links" >}})
 - [List of Service URLs]({{< relref "/prod_urls" >}})

## How things fit together
 - [List of Topic Tags]({{< relref "/tags" >}})
 - [List of Service Areas]({{< relref "/area" >}}) (will be replaced with "components")
 
 ## Diagrams
 - [Full Dependency Diagram](/knowledge_base/rendered/dependencies_full.pdf) ([SVG version](/knowledge_base/rendered/dependencies_full.svg)) - Including links to the knowledge base.
 - [Basic Dependency Diagram](/knowledge_base/rendered/dependencies.pdf) ([SVG version](/knowledge_base/rendered/dependencies.svg)) - The whole graph, minimal labels.

