---
related_services: []
repo_links: []
tags: []
title: Filing Issues
weight: 0
---



Issues for Crossref-managed properties and applications should be filed into GitLab. GitLab issues replace both Jira tickets and GitHub issues.

GitLab issues must be filed into a specific GitLab project within the [Crossref group](https://gitlab.com/crossref). Defects (bugs) for specific applications should be filed into that application’s project. Feature requests should be filed into the [user_stories project](https://gitlab.com/crossref/user_stories/). All other issues, including reporting requests, questions, and developer tasks should be filed into the [Issues project](https://gitlab.com/crossref/issues).

If you’re not sure where to file an issue, [file it to the Issues project](https://gitlab.com/crossref/issues/issues/new) and the triage committee will route it to the appropriate project.

# How to write a good issue

Include as much information as you can when drafting an issue. The triage and Backlog Refinement processes will refine an issue before it is picked up by a developer, so don't worry about getting everything perfect. 

Be careful not to include personally identifiable information in an issue, as GitLab issues are publicly accessible (more on that below).

When reporting a defect, first search for an existing issue reporting the same defect. If an issue already exists, add your instance as a comment to that issue. When filing a new issue or adding to an existing issue, please include as much of the following as possible:
* Steps to reproduce
* Screenshots
* Error messages
* Console logs
* OS, browser and version
* Your assessment of severity/urgency

When filing a feature request into the user_stories project, you'll notice issue descriptions start with a template. Prior to developing new features, we aim to consider the impacts there might be on other aspects of the business. While product managers and stakeholders are responsible for thinking through all of the knock-on effects of our work, any information you can provide on the following topics will help us maintain a healthy software development process:
* Billing/costs
* Internal documentation
* External documentation
* Schema
* Metadata Outputs
* Operations
* Support & Membership experience
* Outreach & Communications
* Testing
* Internationalization
* Accessibility
* Metrics, analytics, reporting

GitLab features a number of controlled fields for issues, including Milestones, Due dates, Labels, Weights, and Confidentiality. We're still evolving how we use a number of these fields, but please note that Milestones and Weights are used for sprint management. Any information provided in these fields will likely be replaced during Backlog Refinement. If your issue is time-sensitive, please let us know in the issue description.


# Triage

All new GitLab issues will be triaged daily, Monday through Thursday, by the triage committee. The triage committee is comprised of product managers (Kirsty and Patrick), Heads of Software and Infrastructure (Joe W and Joe A), support (primarily Isaac), with frequent consultation from Chuck and Geoffrey. 

Triage is intended to cast a wide net, reviewing all newly filed issues regardless of project. Issues may be routed into a few different queues, as appropriate:
* Support - questions from the general public requiring a support response
* Product - feature requests for consideration by the product team
* Needs definition - Issues that require further discussion during Backlog Refinement, before they are ready to be brought into a sprint
* Current sprint - urgent issues may be fast-tracked into the current sprint
* Out of scope - some GitLab issues may be out of scope for the development workflow, eg Strategic decisions register issues

Please note that under normal circumstances, issues will need to be reviewed and slotted into an upcoming sprint. If an issue represents something requiring urgent attention, please say so in the issue, and we'll aim to bring it into the current sprint. If an issue is so urgent that it cannot wait for our daily triage, please file an issue and raise it in the #operations Slack channel.

# Backlog Refinement

High priority issues will be reviewed in more detail by the Backlog Refinement team (presently, the same as the triage committee). During Backlog Refinement, issues will be prepared to meet our definition of ready, estimated, and prioritized into upcoming sprints. 

Backlog Refinement will typically take place three times per sprint on Tuesdays and Thursdays, skipping days on which have Sprint Planning. 

# GitLab issues are public

GitLab issues are accessible to the public by default. We hope this allows us to more tightly align our development work with our community’s needs. 

Please keep this in mind when writing and commenting on issues. Snark, sarcasm, or other creative tones of voice should be avoided. 

Sometimes issues must include personally identifiable information, sensitive information like machine addresses or passwords, or other information we need to keep confidential. In these cases, individual [issues can be marked as confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html), preferably when the issue is drafted by checking the box below the issue description, or after an issue is created by clicking “edit” under the Confidentiality section of the sidebar on the right, then clicking “Turn On.”
