---
area: reporting-monitoring
components: ''
datadeps:
- ext-doi
desc: Generate reports and send to members by email.
docs: []
lang: Java
lead: jstark
legacy: true
owner: ''
packages:
- org.crossref.qs.resolutionreport.DoiDataController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/internal/resolutionreport/doidata
- https://data.crossref.org/members_only/resolutionReport
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- get-doi-db-record
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Resolution reports
userfacing: true
---



Generate reports and send to members by email.
