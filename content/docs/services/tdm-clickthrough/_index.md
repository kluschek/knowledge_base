---
area: infrastructure-apps
components: ''
datadeps:
- infra-datacenter-vanilla
desc: Click-Through Service for TDM agreements.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/clickthrough
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Text and Data Mining (TDM) Clickthrough service (Deprecated)
userfacing: true
---



Click-Through Service for TDM agreements.
