---
area: external
components: ''
datadeps: []
desc: SCOPUS Title List from Elsevier
docs: []
lang: Excel
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.elsevier.com/?a=91122&origin=sbrowse&zone=TitleList&category=TitleListLink
products: []
related_services:
- rest-api-cayenne
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- subject-codes
title: SCOPUS Title List
userfacing: true
---




This is used by Cayenne.