---
area: reporting-monitoring
components: ''
datadeps:
- ext-doi
desc: Generate golive and stats reports and push content to website.
docs: []
lang: Java
lead: jstark
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/statusReport.html
- https://data.crossref.org/reports/50go-live.html
products:
- metadata-retrieval
related_services: []
repo_links:
- https://https://gitlab.com/crossref/golive-status-report
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: GoLive Status report
userfacing: true
---
Generate golive and stats reports and push content to website.
