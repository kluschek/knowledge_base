---
area: registries
components: ''
datadeps:
- human-direct-edit
desc: Crossref deposit schemas and examples. Note that these are not currently automatically
  updated in the Deposit Processor, so need to be manually updated.
docs: []
lang: JSON
lead: pfeeney
legacy: true
owner: pfeeney
packages: []
prod_heartbeats: []
prod_urls:
- https://www.crossref.org/schemas/clinicaltrials.xsd
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/schema
sentry_url: ''
servicedeps:
- infra-gitlab
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- clinical-trial-numbers
title: Crossref Schemas
userfacing: true
---



Crossref deposit schemas and examples. Note that these are not currently automatically updated in the Deposit Processor, so need to be manually updated.
