---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system/blob/develop/java/org/crossref/qs/view/DoiInfoView.java
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- xml-transformation
- crm-items
title: DOI Info Views
userfacing: false
---



This view is used to render XML metadata. 

## CRM Items

The XML is taken from the CDDB. On top of this, items are added to the XML as `crm-item` elements.

CRM items can be one of the following types:

 - string
 - number
 - date

### `doi`

Taken from the DOI of the Citation Reference.

### `citation-id`

Taken from the Citation ID of the Citation Reference.

### Titles

There is a different field to record value under different circumstances. 

#### `journal-title`

If the type of the item is one of:

   - `JOURNAL_TITLE`
   - `JOURNAL_ISSUE`
   - `JOURNAL_VOLUME`
   - `JOURNAL_ARTICLE`

Taken from the Journal Title associated with the item.

#### `conf-series-title` and `conf-vol-title`
   
If the type of the item is one of: 

   - `CONFERENCE_TITLE`
   - `CONFERENCE_SERIES`
   - `CONFERENCE_PAPER`
  
Taken from the Conference Series Title and Conference Volume Title. 

#### `book-series-title` and `book-vol-title`

If the item type is one of:

 - `BOOK_TITLE`
 - `BOOK_SERIES`
 - `BOOK_CONTENT`
 
These are taken from the Book Series Title and Book Volume Title respectively.

#### `component-title`

If the type of the item is `COMPONENT`. This is taken from the XML document using the XPath:

    //x:component/x:titles/x:title
     
#### `dissertation-title`

If the type of the item is `DISSERTATION`. This is taken from the Dissertation Title.

#### `report-series-title` and `report-vol-title`

If the type of the item is one of:

 - `REPORT_PAPER_TITLE`
 - `REPORT_PAPER_SERIES`
 - `REPORT_PAPER_CONTENT`
 
These are taken from the Report Series Title and Report Volume Title respectively.

#### `standard-series-title`

If the type of the item is one of:

 - `STANDARD_TITLE`
 - `STANDARD_SERIES`
 - `STANDARD_CONTENT`
 
These are taken from the Standard Series Title and Standard Volume Title respectively.

#### `database-title`

If the type is `DATABASE_TITLE`. Taken from the Database Title.

#### `dataset-title`

If the type is `DATASET`. This is taken from the dataset title.

### `journal-cite-id`

If the item has a Journal ID.

### `book-cite-id`

If the item has a Book Cite ID.

### `series-id`

If the item has a Series Id.

### `deposit-timestamp`

The "deposit version" timestamp supplied by the depositor.

### `owner`

The owner prefix.

### `last-update`

Formatted `yyyy-MM-dd HH:mm:ss` of the last updated timestamp. Timezone not explicit.

### `prime-doi`

If this has a prime citation ID. 

### `citedby-count`

The 'cited by' / `forward link` count.