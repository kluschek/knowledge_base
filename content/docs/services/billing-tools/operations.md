---
related_services: []
repo_links: []
tags: []
title: Similarity Check Billing Operations
weight: 0
---



---
title: Deposit Billing Operations
---

## Intacct bulk data upload process for Annual Turnitin billing

**For Annual Turnitin billing:**

- Acquire the CSV file provided by Turnitin. (See 1Password). Download the annual billing file.
- Upload the CSV file to billing tools servlet (If not up, it can be spun up in docker container [details to follow]) and save the converted file and upload it to inf3 (Put the file into the /home/crossref/intacct/uploads/ directory.) 
- ssh to inf3 (see crossref entry in 1Password)
- Start a screen to run the processes in if it doesn’t already exist. (screen –R intacct)
- cd to /home/crossref/intacct/ 
- Run the PERL script to convert the CSV file (note n is quarter number [1-4]): 

```text
> perl quarterlyDepositsToIntacctCsv.pl uploads/Qn_billing.csv uploads/Qn_billing_intacct.csv
```
- Run the following command (Note: XREF-COPY is sandbox, XREF is production)[This process will take a while so take a break and come back in an hour or two.]: 
```text
> php -q create_sotransaction_bkroundprocess4.php Qn_billing_intacct.csv XREF|XREF-COPY xml_gw_crossref PASSWORD "CrossRef - US" > Qn_results.html
```
- Login to www.intacct.com (See 1Password)

- Select Order Entry->Records->Membership Invoice->List<br>
    Now you are viewing the uploaded data


<br><br>
**Background information:**

CSV Template:

Notes on the upload template.

The following sample includes the header line of the CSV file, a line showing a sample of the Membership Invoice records used for annual billing, and a line with a sample Sales Invoice from a quarterly billing.

transactiontype,datecreated,createdfrom,customerid,documentno,referenceno,termname,datedue,message,shippingmethod,shipto_contactname,billto_contactname,shipto,billto,currency,basecurrency,exchratedate,exchratetype,exchrate,vsoepricelist,bundlenumber,itemid,itemdesc,warehouseid,quantity,unit,price,discsurchargememo,locationid,departmentid,memo,revrectemplate,revrecstartdate,revrecenddate,renewalmacro,l_projectid,l_customerid,l_vendorid,l_employeeid,l_classid,st_description,st_total,st_percentval,st_locationid
Membership Invoice,12/3/2010,,AAFP00,MI-AAFP00-20101203083906953,,,1/31/2011,CrossRef Annual Fee,,,,,,USD,USD,12/3/2010,Intacct Daily Rate,,,,40000,Annual Member Fee,,1,EA,275,,CrossRef - US,,CrossRef Annual Fee,r12 - Jan - Dec,1/1/2011,12/31/2011,c12 - Jan - Dec,,,,,,,,,
Sales Invoice,9/30/2010,,PLOS00,SI-PLOS00-201012814759,,,10/31/2010,7/2010 : 10.1371 : CY journal deposits (user: plos),,,,,,USD,USD,12/8/2010,Intacct Daily Rate,,,,40801,7/2010 : 10.1371 : CY journal deposits (user: plos) [price 1.0 per unit],,721,EA,721,,CrossRef - US,,7/2010 : 10.1371 : CY journal deposits (user: plos),r12 - Jan - Dec,1/1/2011,12/31/2011,c12 - Jan - Dec,,,,,,,,,

- The header line contains the defined fields required for the upload tool. Not all fields are required to be populated as the examples suggest.
- The first column, transactiontype, must be a type defined in the intacct system. See Order Entry->Order Entry Transaction Definitions.
- Datecreated = invoicedate, not transaction date. Important for quarterly billing.
- The createdfrom field is used to convert records. The transactiontype would contain the type being converted too. The createdfrom would contain the transactiontype-documentno of the original - record being converted. All other data fields must match the existing record in the intacct system. 
- Customerid is the equivalent of our old peachtree id. The customerid must be defined in Order Entry->Customers
- Documentno must be unique.
- Termname and datedue fields should not both be populated. If you want to assign the payment due date, just use the datedue field. If you want it calculated in intacct, you can use a termname assuming you know a defined termname that applies for this payment. The existing n30 termname generates a due date 30 days after the datecreated. See Accounts Receivable->AR Terms 
- Currency and basecurrency leave as USD.
- Exchratedate leave same as datecreated. Not sure this field is even needed since no conversion is being done.
- Exchratetype leave as Intacct Daily Rate. Not sure this is necessary either.
- Itemid is a code for the purchased item or service. Items are defined in Order Entry->Items
- Quantity is the count of deposits of itemid type.
- Price is the quantity multiplied by the price assigned to the itemid definition in Order Entry->Items
- Locationid should be CrossRef – US. Must be a value that exists in intacct Company->Entities
- Revrectemplate must match a template defined in Accounts Receivable->Revenue Recognition Templates
- Revrecstartdate appears to be the start date of the defined template period
- Revrecenddate appears to be the end date of the defined template period
- Renewalmacro from macro definitions in Order Entry->Renewal Macros
<br><br>

