---
area: reporting-monitoring
components: ''
datadeps:
- ext-turnitin-billing
desc: Generate annual billing files for Similarity Check for upload into Intacct system.
  Derived from files sent by TurnItIn.
docs: []
lang: ''
lead: jstark
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- similarity-check
related_services: []
repo_links:
- https://gitlab.com/crossref/billing_tools
sentry_url: https://sentry.io/organizations/crossref/issues/?project=2095999
servicedeps:
- infra-datacenter-docker
- ext-turnitin-billing
- ext-intacct
sonar_url: https://sonarcloud.io/dashboard?id=15286077
staging_heartbeats: []
staging_urls: []
tags:
- billing
- similarity-check
title: Billing Tools (Simcheck)
userfacing: false
---

Generate annual billing files for Similarity Check for upload into Intacct system. Derived from files sent by TurnItIn.
