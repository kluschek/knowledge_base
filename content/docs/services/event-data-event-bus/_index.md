---
area: infrastructure-apps
components: ''
datadeps:
- event-data-agents
- event-data-decanter-agent
- event-data-importer
- event-data-kafka-pusher
- ext-datacite
desc: Event Bus for Event Data. All Events pass through here. Serves as an archive
  and a distribution pipeline.
docs: []
lang: ''
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats:
- https://bus.eventdata.crossref.org/heartbeat
prod_urls:
- https://bus.eventdata.crossref.org
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_event_bus
sentry_url: ''
servicedeps:
- infra-aws
- infra-aws-docker
- infra-aws-kafka
- event-data-docker-base
- util-clojure
- infra-aws-cloudwatch
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Event Bus
userfacing: true
---



Event Bus for Event Data. All Events pass through here. Serves as an archive and a distribution pipeline.
