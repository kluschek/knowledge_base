---
area: tools-libraries
components: ''
datadeps: []
desc: A command line tool for uploading submission files to Crossref's deposit system.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/submission-upload-tool
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Submission Upload Tool
userfacing: true
---

A command line tool for uploading submission files to Crossref's deposit system.
