---
area: tools-libraries
components: ''
datadeps:
- cddb
- rest-api-cayenne
desc: Create bulk snapshots of XML and JSON metadata. Run once a month.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/json_snapshots_tool
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- rest-api-cayenne
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags:
- snapshots
title: Snapshot Generation
userfacing: false
---




The Snapshot Service enables members to download the bulk of Crossref's metadata in either an XML or a JSON encoding. The snapshot is stored in an AWS S3 bucket. Snapshots are organized by date, encoding, and document type. The organization can be navigated any anyone with a browser but snapshots can only be downloaded by Plus members.


