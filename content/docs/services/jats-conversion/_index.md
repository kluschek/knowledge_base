---
area: etl
components: ''
datadeps:
- jats-xslt-cs
desc: API to convert JATS to XSLT
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.ds.jatsconversion.JatsToCrossrefController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/service/jatsconversion
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- jats-xslt-cs
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- jats
title: JATS to XML converter API
userfacing: false
---




This uses an XSLT file. Although this file is available in a separate repository, the one used is stored at `WEB-INF/xslt/JATS2CrossRef_web.xsl` in the Content System repository. 
