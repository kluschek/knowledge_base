---
area: distribution-querying
components: ''
datadeps: []
desc: Upload simple text queries
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.simpletextquery.SimpleTextQueryUploadController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/stqUpload/**
products:
- metadata-search
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- authorization-service
- submission-upload
- deposit-queue
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- simple-text-query
title: Simple Text Query Upload Service
userfacing: false
---




Content to follow.

