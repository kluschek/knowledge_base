---
area: reporting-monitoring
components: ''
datadeps:
- rest-api-cayenne
- get-prefix-publisher
- title-search
desc: Generate data for the Participation Reports UI.
docs: []
lang: java
lead: myalter
legacy: true
owner: kmeddings
packages:
- org.crossref.participation.reports.ParticipationReportDataController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/prep/data/**
- https://doi.crossref.org/prep-staging/data/**
products:
- participation-reports
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-tomcat
- get-prefix-publisher
- rest-api-cayenne
- title-search
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Participation Reports Middleware
userfacing: false
---



Generate data for the Participation Reports UI.
