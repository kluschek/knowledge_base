---
area: external
components: ''
datadeps:
- deposit-billing
desc: Billing file supplied annually by TurnItIn
docs: []
lang: ''
lead: jstark
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: TurnItIn Billing File
userfacing: false
---



Billing file supplied annually by TurnItIn
