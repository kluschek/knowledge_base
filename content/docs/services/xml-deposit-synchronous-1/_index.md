---
area: ingestion
components: ''
datadeps: []
desc: Deposit via REST API `/v1/deposit`. This is a profile of the Cayenne REST API
  service.
docs: []
lang: Clojure
lead: ''
legacy: true
owner: ppolischuk
packages: []
prod_heartbeats:
- https://api.crossref.org/v1/heartbeat
prod_urls:
- https://api.crossref.org/v1/deposit
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/rest_api
sentry_url: ''
servicedeps:
- infra-datacenter-mongo
- infra-solr
- infra-datacenter-elastic
- infra-email-inbox
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- submission
title: Synchronous REST API Deposit v1
userfacing: true
---



Deposit via REST API `/v1/deposit`. This is a profile of the Cayenne REST API service.
