---
area: distribution-querying
components: ''
datadeps: []
desc: Content Delivery Network specifically for caching CrossMark assets for use in
  members' sites.
docs: []
lang: AWS
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://crossmark-cdn.crossref.org
products:
- crossmark
related_services: []
repo_links:
- https://github.com/CrossRef/crossmark
sentry_url: ''
servicedeps:
- infra-cloudfront
- crossmark-dialog
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- cdn
- cache
title: CrossMark CDN
userfacing: false
---



Content Delivery Network specifically for caching CrossMark assets for use in members' sites.
