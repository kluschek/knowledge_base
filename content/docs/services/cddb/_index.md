---
area: infrastructure-apps
components: metadata-repository
datadeps:
- deposit-processor
desc: Internal distribution of XML blobs for use in various parts of the system.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-retrieval
- metadata-search
- reference-linking
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-berkeleydb
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref DOI Database (CDDB) System
userfacing: false
---



Internal distribution of XML blobs for use in various parts of the system.

There is a `getCitationReference` and a `findCitationReference`. The latter returns null if none found, the `get` form excpects that the caller knows the record is there and will attempt to retrieve the data from Oracle if the DOI could not be found, and throws an exception if not found in Oracle. 

