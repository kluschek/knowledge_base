---
related_services: []
repo_links: []
tags: []
title: Assets CDN operations
weight: 0
---



## To add a file

Ask a developer to help.

1.  Go to the [Assets project](https://gitlab.com/crossref/assets)
2.  Create a branch with the new file and a merge request.
3.  Review the new file(s) for suitability.
4.  Merge to master. The CI build automation will upload and refresh the CDN.
5.  Check that the automation ran and didn't fail, then check the URL at https://assets.crossref.org to see if it worked OK.
6.  Update the instructions on how to use the file, e.g. website brand page.