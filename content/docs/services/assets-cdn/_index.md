---
area: distribution-querying
components: ''
datadeps:
- human-direct-edit
desc: Content Delivery Network from which we serve images for our applications, websites
  and our users.
docs: []
lang: ''
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://assets.crossref.org
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/assets
sentry_url: ''
servicedeps:
- infra-cloudfront
- infra-s3
- infra-gitlab
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- cdn
title: Assets CDN
userfacing: true
---




The Assets CDN (Content Delivery Network) is a place where we store files, usually images, that are used by our members. For example, our logos. The CDN is effectively a high-performance web server which means that it offers fast load-times. By hosting the logos and requiring that users link to them, rather than allowing them to copy, we can update the files if neccessary.

**Asset files should never be copied off the CDN by our members, it's designed for them to 'hotlink' directly to the file.**

By putting a file on a CDN we make a commitment to our users that it will never be removed, and that they can rely on it. Therefore the process of adding new files needs to be controlled and thought-through.

## Suitability review

1. Will you want this file to be here one year from now? If not, why not?
2. What process is used for amending it in future? Who product manages it? Does it fall under a project plan?
3. If it's an image, will the dimensions change in future? If so, this will prevent people from using it reliably.
4. Is it for public use? How do they find it? The Brand pages on the website should probably be updated to reference it, and tell people how to use it.
5. How big is the file? It should be web-optimised / compressed for size.




