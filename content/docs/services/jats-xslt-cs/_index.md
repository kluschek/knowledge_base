---
area: tools-libraries
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- WEB-INF/xslt/JATS2CrossRef_web.xsl
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- jats-xslt
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- jats
title: JATS XSLT converter (in Content System)
userfacing: false
---




This is a copy of the JATS to XML conversion XSLT file, kept in the CS repository.
