---
area: ingestion
components: ''
datadeps: []
desc: ''
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/servlet/submissionAdmin?sf=showQueries
- https://doi.crossref.org/servlet/submissionAdmin?sf=doiQuery
- https://doi.crossref.org/servlet/submissionAdmin?sf=citedByLinks
- https://doi.crossref.org/servlet/submissionAdmin?sf=links
- https://doi.crossref.org/servlet/submissionAdmin?sf=query
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Admin Console Query
userfacing: false
---

## Interactive Query Upload

## DOI Query

Accepts a list of DOIs, returns metadata in one of:

 - Piped
 - UNIXSD
 - UNIXREF
 - XSD_XML (deprecated)
 


## Cited By Links

Accepts a DOI, returns list of citations.