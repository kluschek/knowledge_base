---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.citationsearch.AuthorizedCitationSearchService
- org.crossref.qs.citationsearch.CitationSearchCoordinator
- org.crossref.qs.citationsearch.SimpleCitationSearchService
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- authorization-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Citation Search Service
userfacing: false
---




TODO