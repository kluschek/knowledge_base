---
area: tools-libraries
components: ''
datadeps: []
desc: JATS to CrossRef deposit XML translation via an XSLT. Available to the public,
  also can be used in deposit processing.
docs: []
lang: ''
lead: pfeeney
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/jats-crossref-xslt
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- jats
title: JATS / Crossref XSLT
userfacing: true
---



JATS to CrossRef deposit XML translation via an XSLT. Available to the public, also can be used in deposit processing.
