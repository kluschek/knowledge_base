---
area: infrastructure-services
components: ''
datadeps: []
desc: GitLab as a platform for hosting files, running CI or issue tracking.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: GitLab
userfacing: false
---



GitLab as a platform for hosting files, running CI or issue tracking.
