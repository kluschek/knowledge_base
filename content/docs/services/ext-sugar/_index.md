---
area: external
components: ''
datadeps:
- member-info
desc: SugarCRM. System for managing members' accounts.
docs: []
lang: ''
lead: jstark
legacy: true
owner: abartell
packages: []
prod_heartbeats: []
prod_urls:
- https://crossref.sugarondemand.com
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://crossreftest.sugarondemand.com
tags: []
title: Sugar CRM
userfacing: true
---

SugarCRM. System for managing members' accounts.

This is administered by Crossref staff. We work with Faye Systems to administer our Sugar installation.

Member accounts are managed here. Data is synchronised between Sugar and CS via MemberInfo, in both directions.

Data is sent from Sugar to our Intacct financial system when new accounts are set up in Sugar that require billing.
