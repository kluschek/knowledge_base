---
area: distribution-querying
components: ''
datadeps: []
desc: Data Access for OAI-PMH and other internal services.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-plus
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: OAI-PMH DAO
userfacing: false
---

Data Access for OAI-PMH and other services.
