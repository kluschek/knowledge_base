---
area: infrastructure-services
components: ''
datadeps: []
desc: Webserver for serving content, files and PHP applications.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Apache Server
userfacing: false
---



Webserver for serving content, files and PHP applications.
