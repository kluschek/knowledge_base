---
area: ingestion
components: ''
datadeps:
- member-info
- authorization-service
- submission-upload
desc: Standard XML Deposit API used by our members. Uses filesystem.
docs: []
lang: Java
lead: ''
legacy: true
owner: ppolischuk
packages:
- org.crossref.ds.submissionupload.SubmissionUploadCoordinator
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- crossref-schemas
- infra-s3
- infra-oracle
- infra-datacenter-mysql
- authorization-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- submission
title: XML Deposit System
userfacing: true
---




Includes Submission Upload Coordinator.