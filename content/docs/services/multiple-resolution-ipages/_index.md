---
area: distribution-querying
components: ''
datadeps:
- cddb
desc: Serve up Interrim Landing Pages for Co-access. Templates are created and stored
  in source control.
docs: []
lang: ''
lead: ''
legacy: true
owner: kmeddings
packages:
- org.crossref.qs.controllers.IPageController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/ipage/**
- https://doi.crossref.org/iPage/**
products:
- multiple-resolution
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-tomcat
- cddb
- handle-manager
- citation-reference-finder
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Multiple resolution ipages
userfacing: true
---



Serve up Interim Landing Pages for Co-access. Templates are created and stored in source control.

**Support documentation (background, launching new templates)**

* Support docs: https://support.crossref.org/hc/en-us/sections/203041646
* Website service page: https://www.crossref.org/services/content-registration/multiple-resolution/
* Website get started guide: https://www.crossref.org/get-started/multiple-resolution/
* Website FAQs: https://www.crossref.org/faqs/multiple-resolution/


**What is it**

Multiple resolution is a feature of the DOI resolver that allows multiple URLs to be attached to a single DOI. Crossref’s implementation of MR directs the resolver to a Crossref-hosted “interim page” which imports all URLs registered with the DOI into the page as either clickable images, linked text, or the URL itself.

**How does it work**

When more than one URL is added to a DOI, the DOI resolver is instructed to resolve to an interim page hosted by a registration agency.  The URL registered with the initial record (or in the initial <resource> tag) is the ‘primary’ URL. All additional URLs are ‘secondary’ URLs.

For example:
https://doi.org/10.1177/152216280100400122

In the Handle record (http://handle.net )  the 10320/loc value is:

<locations chooseby="locatt,country,weighted"><location id="1" cr_type="MR-LIST" href="http://www.crossref.org/iPage?doi=10.1177%2F152216280100400122" weight="1" /><location id="2" cr_src="clockss_su" label="CLOCKSS_SU" cr_type="MR-LIST" href="http://triggered.stanford.clockss.org/ServeContent?rft_id=info:doi/10.1177/152216280100400122" weight="0" /><location id="3" cr_src="clockss_edina" label="CLOCKSS_EDINA" cr_type="MR-LIST" href="http://triggered.edina.clockss.org/ServeContent?rft_id=info:doi/10.1177/152216280100400122" weight="0" /><location id="4" cr_src="nlnether" label="KB_fulltext" cr_type="MR-LIST" href="http://resolver.kb.nl/resolve?urn=urn:nbn:nl-kb:edepot:ewtij:1228472125213" weight="0" /></locations>

This tells the DOI resolver to resolve the DOI to:

http://www.crossref.org/iPage?doi=10.1177%2F152216280100400122

The other registered URLs are recorded as well. The DOI resolver can be instructed to bypass multiple resolution as described here.

**Template specifics**

A template should be in place before secondary URLs are registered.  The template is an HTML page containing flags that note where a piece of metadata or URL should be inserted.

Flags include:
```
<!--doi--> will be replaced by the DOI
<!--metadata--> will be replaced with metadata from the registered metadata record for the DOI. This data is presented as HTML table rows and data cells
<!--link--> will be replaced by URL of one MR target (multiple uses of this directive will iterate through the available MR secondary targets)
<!--link-prime--> will be replaced by the primary URL (presumably the content owner's URL)
<!--link label="XYZ123"--> will be replaced by the URL of the MR target deposited with the specified label. Important: Labels used in <!--link label="XYZ123"--> must match the label supplied in deposits by the secondary depositor.
```


**Labels**

Labels are custom values used to associate a URL with a link in the interim page template.  The primary URL is always labeled ‘link-prime’.  The label value is included in secondary URL deposits and must match a label provided in a template - if the label is not present, the secondary URL won’t be imported into the template.  Labels are case sensitive, characters are limited to letters numbers and _

**Image, CSS, and javascript files**

Members can (and do) supply image and CSS files for their templates. These files must follow a specific file path to be pulled into the template:

Images: /iPage/docs/images/example.jpg
CSS: /iPage/docs/css/example.css
Javascript:  /iPage/docs/js/example.js

Most templates have the wrong file path.  We need to edit the path to make sure the images and CSS are imported.

**Checking in templates**

When a member supplies a template, we need to check it (and any associated image or css files) in to the system. After the template is checked in, a full deployment is needed to make the template live.

Templates can be checked in by Isaac. More details on updates themselves:

template (a .html file) placed in:
web/WEB-INF/iPage/

image files (.jpg, .png, .gif) placed in:
web/docs/iPage/images/

CSS files placed in:
	web/docs/iPage/css/

If the file has a generic name (style.css or logo.jpg, for example) you may want to change the name to something more distinctive to avoid overwriting existing files supplied for other members. Be sure to update the template or .css file to reflect the change.

**Steps for deploying new/updated multiple resolution templates, images to Gitlab using NetBeans**

1. Create a new Gitlab issue for the multiple-resolution-related changes being made
1. When ready to update your template/images, switch to the develop branch in NetBeans and do a pull so that your instance is updated with the latest in develop: right click on a file > Git > Branch/Tag > Switch to Branch; then right click > Git > Remote > Pull > develop
1. Create a new branch for these changes: right click on file, Git > Branch/Tag > Create Branch
1. Make necessary changes to templates (html), css, images
1. Commit changes: Git > Commit > in the commit message, must reference the Gitlab issue that was created in step 1 in the commit message, like this: ‘crossref/issues#636 - PDC new css’
1. Push changes: Git>Remote>Push>select branch that was created in step 3 and push it
1. Find pushed changes in Gitlab; create a merge request for the changes and then deploy


**Associate DOIs with a template**

A template can be associated with an entire DOI prefix or with a single title.  The title template overrides the prefix template. The ‘title’ is determined by journal or book cite ID and is set up in the system admin.

To associate a template with a specific book title go to Metadata Admin > Multi Resolution and click on the “Create” button. Enter the filename used in the template that was checked in. Enter the primary publisher’s prefix and the book’s citation (or, Title) ID, select the box next to “is Book:” and create. 

**Testing templates**

There is no good way to do this before the secondary URLs have been registered. We need to deploy the template then ask the member to make an initial secondary URL deposit. Luckily most templates are simple and do not need testing. Once a secondary URL has been deposited, the interim page will be visible. Joe Aparo or another member of the tech team can host the interim page on inf3 to allow preview of the interim page prior to its release. But, the template must first be deployed, as described in the Steps for deploying new/updated multiple resolution templates, images to Gitlab using NetBeans section (above) of this document.

**Deposit permissions**

A member is by default able to supply secondary URLs for their own prefix.  We can also provide multiple resolution-only permissions to secondary depositors. This involves creating a special multiple resolution account and attaching it to a prefix. 
Create MR deposit account by creating username/password and ticking off the ‘multiple resolution’ box.  MR-only accounts are usually a username + _mr (for example muse_mr).
Add the MR deposit account to the appropriate prefix

**Unlocking DOIs**

DOIs must be unlocked by their owner. This is done by supplying an ‘unlock’ flag as described here (link).  The unlock flag can be supplied as part of a secondary URL deposit (if the prefix owner is doing the unlocking).

**Secondary URLs**

A member can supply secondary URLs for their own DOIs as part of a metadata or resource deposit,  or (with permission) allow another member to supply secondary URLs.

```
<doi_resources> 
  <doi>10.50505/mrtest</doi> 
     <collection property="list-based"> 
        <item label="SECONDARY_X"> 
            <resource>http://www.crossref.org/test1</resource> 
        </item> 
     </collection> 
</doi_resources> 
```


A DOI must be registered before secondary URLs can be supplied. This is obvious to us but some members aren’t aware of this.

Deposit details are here: https://support.crossref.org/hc/en-us/articles/215784203

**Updates**

Updates to primary URLs can be done by the usual methods. Secondary URLs must be supplied via secondary depositor.

**Reversing Multiple Resolution**

Members can supply a ‘lock’ flag for each DOI, which will disable MR and remove any secondary URLs. Members may also supply an empty item tag (</item>) :

```
<doi>10.50505/mrtest1</doi>
   <collection property="list-based">
    <item />
   </collection>
  </doi_resources>
```


Details here: https://support.crossref.org/hc/en-us/articles/214353526


