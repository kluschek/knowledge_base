---
area: infrastructure-apps
components: ''
datadeps: []
desc: Queue for controlling the processing of deposit processing. User-facing via
  the per-user admin dashboard.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-oracle
- infra-datacenter-mysql
- infra-datacenter-activemq
- infra-s3
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Deposit Queue System (Q)
userfacing: true
---



Queue for controlling the processing of deposit processing. User-facing via the per-user admin dashboard.
