---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.GetRAController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/doiRA/**
- https://doi.crossref.org/ra/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- ext-doi
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- ref-pref
title: Get Registration Agency
userfacing: false
---



Content to follow.
