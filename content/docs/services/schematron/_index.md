---
area: tools-libraries
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: pfeeney
legacy: true
owner: pfeeney
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/schematron
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Schematron
userfacing: true
---

Served from `ftp-reports` backend.
