---
area: etl
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.metadatacitationsearch.fuzzymatching.FuzzyMatcher
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/fuzzy/**
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Fuzzy Matcher
userfacing: false
---




The `org.crossref.qs.metadatacitationsearch.fuzzymatching.FuzzyMatcher` class contains a lot of text processing rules.

