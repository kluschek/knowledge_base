---
related_services: []
repo_links: []
tags: []
title: ORCID Auto Update Design
weight: 0
---



# Configuration

The context files are

 - orcidRoundtripContext.xml
 - orcidRoundtripServlet.xml

The deployment properties have the prefix

 - qs.orcidroundtrip

# Implementation

OAU is one of the more complicated CS sub-systems due to the workflow. There is much explicit coordination between the process's moving parts. The moving parts are:

 - Making Claims
 - Adding Works
 - Updating Works
 - Requesting Authorization
 - Responding to Authorization
 - Expiring Requests for Authorization
 - Expiring Denials of Authorization

All the code is in the Java packages

 - org.crossref.xs.orcidroundtrip
 - org.crossref.xs.orcidroundtrip.artifacts
 - org.crossref.xs.orcidroundtrip.controllers
 - org.crossref.xs.orcidroundtrip.daos
 - org.crossref.xs.orcidroundtrip.exceptions
 - org.crossref.xs.orcidroundtrip.tasks

# Database Design

The active data is all stored in the MySql "qs" database tables:

## orcid_roundtrip_access_tokens

This holds the OAuth2 access tokens for each author. There are two tokens per author. One giving ORCID read access, scope "/orcid-works/read-limited", and one giving ORCID update access, scope "/activities/update".

## orcid_roundtrip_authorization_status

This holds the request for permission status. Once an author has given Crossref permission then their record here is deleted.

## orcid_roundtrip_authors

This holds the author status. There are 3 explicit states one 1 implied. The explicit states are "denied" where the user has denied us permission, "suspended" where Crossref has denied them making claims, and "locked" where the author has locked out Crossref (and other third parties) from asking permission. The implied status is "ok" and this is when none of the other statuses are set!

## orcid_roundtrip_claims

This holds the claims. Once a claim is processed it is removed. A claim for an author that never intends to acknowledge our request for permission will remain in the table forever.
The usage data is stored in the MySql "usage" database in the tables

## orcid_roundtrip_addedworks_YYYYMM

These tables hold the records of added claims.

## orcid_roundtrip_updatedworks_YYYYMM

These hold the records of updated claims.

# Making Claims

A claim is that an author contributed to a work. At this time, we only make claims for authorship and not editing a work, for the first 10 authors (this limit comes from the deposit system, see CS-3680), and
not for databases, standalone components, nor peer reviews.

A claim is made by calling `makeClaim()` on the `orcidroundtripService` bean. After some verification a `org.crossref.xs.orcidroundtrip.tasks.ClaimInsertTask` instance is created and the claim is, indirectly, added to the orcid_roundtrip_claims table. It is not an error to have the duplicates of a claim in the table; removing duplicates is done during the adding or updating of works.

# Make Claims via JMX

The mbean "qs.orcidroundtrip:name=orcidroundtripService" operation "makeClaim" can be used to make a claim. The "work-id" parameter can be a citation id or a DOI.

# Make Claims via HTTP

The org.crossref.xs.orcidroundtrip.controllers.MakeClaimContoller controller is mapped to the path "/internal/claim" and expects parameters "orcid", and either "doi" or"citationid". No credentials are needed.

# Adding & Updating Works

Adding and updating works is a scheduled task. During initialization the "orcidroundtripService" bean creates several instance of `org.crossref.xs.orcidroundtrip.tasks.AddWorksTask`

These instances are scheduled to awake twice a minute and 

 - Get a list of new claims -- priority and standard claims.
 - Get a list of claims previously made from ORCID.org.
 - Process each new claim as an update or an addition depending on whether the claim was previously made or not, respectively. See classes `org.crossref.xs.orcidroundtrip.tasks.AddWorkTask` and  `org.crossref.xs.orcidroundtrip.tasks.UpdateWorkTask`

The schedule is defined as a cron expression in the deployment property

    qs.orcidroundtrip.orcid.add-works-schedule=0,30 * * * * *

There are several instances of AddWorksTask where each instance handles all the claims for a group of ORCID ids (a shard). The priority claims are processed before the standard claims. A priority claim is one where the author has just granted Crossref permission to update their record and so we want them to see their works as soon as possible.

ORCID ids are grouped by a bitmask applied to the ORCID id's "number." This number is the base-10 equivalent of the concatenation of the ORCID's digits. Eg, for the ORCID id 1234-5678-9101 the "number" would be 123,456,789,101. 

# Requesting Authorization

Requesting authorization is a scheduled task. During initialization the "orcidroundtripService" bean creates several instance of `org.crossref.xs.orcidroundtrip.tasks.AuthorizationRequestsTask`

These instances are scheduled to awake three times a minute and for each author who has yet to be contacted it sends them an ORCID notification requesting permission to update their ORCID record. A different message is used if this is the first or a subsequent notification. The messages are defined in the "orcidroundtripNotificationMessageDao" bean. These should only be modified as part of the product management process.

The interaction with ORCID.org's API is done in the Java class `org.crossref.xs.orcidroundtrip.tasks.AuthorizationRequestTask`

The schedule is define as a cron expression in the deployment property

    qs.orcidroundtrip.orcid.request-authorization-schedule=0 20,50 * * * *

As with adding records, each `AuthorizationRequestsTask` instance handles a set of ORCID ids.

## Responding to Authorization

ORCID.org uses the OAuth2 protocol for authorization. This protocol has 3 steps

1. The client requests an authorization request to the server for one or more access scopes.
When authorization is granted (an internal process to ORCID.org), the client must get an access token.
2. The server sends an authorization code to the server (via a callback URL). (The server can also send a denial.)
3. The client uses the authorization code to request the access tokens for the scopes.
These last two steps must be done within a few seconds. In the end, Crossref has two access tokens for use with the author's ORCID record.


Once Crossref has permission we prioritize adding the author's claims as soon as possible.

An authorization request to ORCID.org contains a callback URL. This URL is used by ORCID.org when the author grants permission for Crossref to update his or her record. The callback URL is defined in the deployment property `qs.orcidroundtrip.authorizationcode.redirecturl`

When the callback is made it is handled by the method `org.crossref.xs.orcidroundtrip.OrcidRoundtripServiceImpl.makeAccessToken()`

This will request the access tokens and then prioritize the processing of the author's claims. If permission has been granted then access tokens are requested using `org.crossref.xs.orcidroundtrip.tasks.AccessTokenRequestTask`

It will add the access tokens to the "orcid_roundtrip_access_tokens" MySql table. 

# Expiring Requests for Authorization

It is Crossref's policy that if an author does not respond to the permission request then another is sent later. The permission request is sent every 60 days; this is defined in the deployment property
qs.orcidroundtrip.orcid.requested-authorization-expiration-interval=60 DAYS
During the initialization of the "orcidroundtripService" Spring bean several instances of the expiration task is created
`org.crossref.xs.orcidroundtrip.tasks.ExpireAuthorizationRequestsTask`

These tasks are run twice a day

    qs.orcidroundtrip.orcid.requested-authorization-expiration-schedule=0 0 11,23 * * *

The task looks for authorizations that are older than 60 days and resets it "requested" flag.

# Expiring Denials of Authorization

It is Crossref's policy that if an author denies permission then another request is sent later. The permission request is sent every 60 days; this is defined in the deployment property
`qs.orcidroundtrip.orcid.denided-authorization-expiration-interval=180 DAYS`

During the initialization of the `orcidroundtripService` Spring bean several instances of the expiration task is created `org.crossref.xs.orcidroundtrip.tasks.ExpireAuthorizationDenialsTask`

These tasks are run twice a day

    qs.orcidroundtrip.orcid.denided-authorization-expiration-schedule=0 0 11,23 * * *

The task looks for denials in the "orcid_roundtrip_authors" table that are older than 180 days and then resets it "denied_authorization" flag and reclaims for the author's works.

see also:

https://gitlab.com/crossref/user_stories/issues/235

