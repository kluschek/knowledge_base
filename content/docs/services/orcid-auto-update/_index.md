---
area: etl
components: ''
datadeps:
- operational-rules
desc: Send notifications to authors' ORCID inboxes.
docs: []
lang: Java
lead: ''
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- orcid-auto-update
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- operational-rules
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags: []
title: ORCID Auto Update System
userfacing: false
---




# Overview

The ORCID Auto Update Service (OAU) adds works to an author's ORCID record when a DOI for that work is deposited with Crossref. An ORCID "work" is similar to Crossref's "registered content" and contains similar metadata. Crossref provides a minimum of the registered content metadata, however. When the registered content metadata is updated then the work is updated too. At present we do not have an automatic means to remove a work when the author is removed from the registered content metadata. (Most often this is due to a mistaken author identification.)

To add a work to an author's ORCID record, the author needs to grant Crossref permission. The ORCID.org webapp has an "inbox" for "notifications." Crossref requests permission via a notification. Once permission is granted then Crossref can indefinitely modify the record until the author revokes permission. An author can also choose to deny Crossref permission; when this happens Crossref will wait for 180 days to pass and then ask for permission again. If the author fails to respond to a permission request within 60 days then another request is made.

OAU is intended to be an unsupervised, continuous background process.

NOTE: OAU was initially called "ORCID Roundtrip." All the code and configuration uses this original name. (The name change came too late in the development to rename the code and configuration.)

