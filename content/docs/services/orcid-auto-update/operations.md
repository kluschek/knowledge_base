---
related_services: []
repo_links: []
tags: []
title: ORCID Auto-Update Operations
weight: 0
---



## Block a claim

If a publisher includes an ORCID in their metadata and the author has opted in to Auto-Update, an ORCID notification is triggered to that person's ORCID inbox asking if they would like to add that DOI to their ORCID profile.

If the publisher has included an incorrect DOI in their metadata, the wrong author will receive this notification and will contact support to ask that they are disassociated from that DOI.

The main way to fix this is to ask the publisher to redeposit the metadata with the correct ORCID or no ORCID.  However this takes time and some publishers do not respond. However using the Blocked ORCID Claims file will allow Support to prevent future notifications from being raised.

### Steps:

1) Edit `blocked-orcid-claims.csv` in the [Rules](https://gitlab.com/crossref/rules/tree/master) Repository on the `master` branch.
2) Add a new DOI, ORCID pair to the list, one line per pair. If there are multiple DOIs for one ORCID (or vice versa) create that many lines. Do not include the full URL of the DOI. If the DOI contains a comma, quote the value, e.g. :
    > `"10.5555/12,34",0000-0000-000-0000`
3) In the commit message link to either the Zendesk ticket or the GitLab issue number.
4) Next time the task to send ORCID notifications runs it will ingest this list and ignore the relevant claims.

### Failure modes:

If the GitLab file is not accessible to the CS instance when it runs, a previously retrieved version will be used. This may be out of date.
