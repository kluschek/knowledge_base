---
area: distribution-querying
components: ''
datadeps:
- event-data-evidence-log
desc: Snapshot and archive daily Evidence Logs that show how Agents and Percolator
  behave. Logs are saved to, and can be retrieved from, Evidence Registry.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-evidence-log-snapshot
sentry_url: ''
servicedeps:
- event-data-docker-base
- infra-aws-kafka
- infra-aws-docker
- infra-s3
- util-clojure
- infra-aws-cloudwatch
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Evidence Log Snapshot
userfacing: false
---



Snapshot and archive daily Evidence Logs that show how Agents and Percolator behave. Logs are saved to, and can be retrieved from, Evidence Registry.
