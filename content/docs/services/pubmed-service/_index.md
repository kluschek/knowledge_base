---
area: legacy
components: ''
datadeps:
- ext-pubmed
desc: Retrieves data from PubMed, including PMIDs
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.util.PubMedService
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: ''
servicedeps:
- ext-pubmed
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- pmid
title: PubMed Service
userfacing: false
---

Retrieves data from PubMed, including PMIDs

## Retrieving PMID based on DOI

Example:

`https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=10.1183/09031936.03.00057003`
