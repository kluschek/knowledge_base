---
area: tools-libraries
components: ''
datadeps: []
desc: Common components for Event Data.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-common
sentry_url: ''
servicedeps:
- util-clojure
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Common
userfacing: false
---



Common components for Event Data.
