---
area: registries
components: ''
datadeps:
- funder-registry-ingester
desc: Registry of Funders stored within Crossref.
docs: []
lang: XML
lead: myalter, jhanna
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/fundingdata
products:
- funder-registry
related_services: []
repo_links:
- https://github.com/CrossRef/open-funder-registry
sentry_url: ''
servicedeps:
- infra-gitlab
- interim-storage
- admin-console
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Open Funder Registry
userfacing: true
---



Registry of Funders stored within Crossref.
