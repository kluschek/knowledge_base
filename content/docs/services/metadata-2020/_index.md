---
area: websites
components: ''
datadeps: []
desc: Metadata 2020 website.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.metadata2020.org/
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/md2020
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Metadata 2020
userfacing: true
---



Metadata 2020 website.
