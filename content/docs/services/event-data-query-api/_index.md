---
area: distribution-querying
components: ''
datadeps:
- event-data-event-bus
- infra-aws-elastic
desc: API for querying and retrieving Events. Also serves Scholix API.
docs:
- https://www.eventdata.crossref.org/guide/service/query-api/
lang: Clojure
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_query
sentry_url: ''
servicedeps:
- event-data-docker-base
- infra-aws-kafka
- infra-aws-docker
- infra-s3
- infra-aws-cloudwatch
- infra-aws-elastic
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Query API
userfacing: true
---



API for querying and retrieving Events. Also serves Scholix API.
