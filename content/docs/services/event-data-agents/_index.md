---
area: collection
components: ''
datadeps:
- event-data-artifact-registry
desc: Collection of Event Data from external sources. These run on a schedule and
  push data into the Percolator. Does not expose an HTTP service, so no direct heartbeat.
  Instead, continual operation is monitored by the Event Data Quality heartbeat.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-agents
sentry_url: ''
servicedeps:
- event-data-artifact-registry
- infra-hetzner-docker
- infra-hetzner-kafka
- infra-s3
- event-data-docker-base
- util-clojure
- infra-hetzner-elk
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Agents
userfacing: false
---



Collection of Event Data from external sources. These run on a schedule and push data into the Percolator. Does not expose an HTTP service, so no direct heartbeat. Instead, continual operation is monitored by the Event Data Quality heartbeat.
