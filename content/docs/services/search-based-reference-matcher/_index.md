---
area: etl
components: ''
datadeps:
- rest-api-cayenne
desc: Search-based reference matcher with validation
docs: []
lang: Java
lead: dtkaczyk
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reference-linking
related_services: []
repo_links:
- https://github.com/CrossRef/search-based-reference-matcher
sentry_url: ''
servicedeps:
- rest-api-cayenne
- crossref-common-java
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Search Based Reference Matcher
userfacing: false
---



Search-based reference matcher with validation
