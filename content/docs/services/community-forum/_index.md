---
area: documentation
components: ''
datadeps: []
desc: Discource-powered community forum
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://community.crossref.org/
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Community Forum
userfacing: true
---




The Crossref Community forum. This is hosted externally on the commercial Discourse servers.

