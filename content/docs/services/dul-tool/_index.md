---
area: tools-libraries
components: ''
datadeps:
- dul-token-repository
desc: Proof of concept and reference implementation of Distributed Usage Logging Message
  Authentication Recommended Specification
docs: []
lang: Java
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- distributed-usage-logging
related_services: []
repo_links:
- https://github.com/CrossRef/dul-tool
sentry_url: ''
servicedeps:
- dul-token-repository
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DUL Tool
userfacing: true
---



Proof of concept and reference implementation of Distributed Usage Logging Message Authentication Recommended Specification
