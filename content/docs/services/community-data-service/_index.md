---
area: infrastructure-apps
components: community-data
datadeps: []
desc: Represent our Membership and Community structures, who they are, and what they
  can do. Necessary for Access Control, billing, self-service and many other functions.
docs: []
lang: java
lead: japaro
legacy: false
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://apps.crossref.org/search/coaccess
- https://apps.crossref.org/coaccess/coaccess.html
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/community_data
sentry_url: ''
servicedeps:
- authenticator
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- authenticated
- auth
title: Community Data Service
userfacing: true
---

Represent our Membership and Community structures, who they are, and what they can do. Necessary for Access Control, billing, self-service and many other functions.

Current functionality:

 - Access Control for credentials
 - Issuance of tokens for internal use
