---
area: etl
components: ''
datadeps: []
desc: Title Matching with the Deposit process
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Title matching
userfacing: false
---



Title Matching with the Deposit process
