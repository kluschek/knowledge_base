---
area: distribution-querying
components: ''
datadeps:
- xml-query
- cddb
- rest-api-cayenne
desc: Check the up-to-dateness of the REST API's database.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-search
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- infra-datacenter-apache
- rest-api-cayenne
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags: []
title: REST API Checker
userfacing: false
---



Check the up-to-dateness of the REST API's database.
