---
area: distribution-querying
components: ''
datadeps: []
desc: Access data about Journals.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.databasedaos.journalcite
prod_heartbeats: []
prod_urls: []
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Book Cite DAO
userfacing: false
---

Data Access for Journals.
