---
area: infrastructure-services
components: ''
datadeps: []
desc: CloudFront CDN
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-s3
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- cache
- cdn
title: Cloudfront
userfacing: false
---




CloudFront is a HTTP CDN that connects to an Origin, whether that's an S3 bucket or an HTTP service that we provide.

CloudFront is used to terminate TLS connections. The wildcard certificate for `*.crossref.org` and `*.eventdata.crossref.org` are updated using the AWS Manager.

## Caching

CloudFront points to one or more endpoints. These can be S3 buckets or HTTP services. Depending on the caching policy specified in the configuration and in the `Cache-Control` HTTP header, the object will be cached in the edge network.

CloudFront provides good performance by choosing a local node for each user by geo-locating their IP address. 

## Geo-location

Depending on the location of the user, different edge caches may make independent requests to the endpoint. If the content changes this can result different users in different locations seeing different content. If users report conflicting experiences, this may be the cause. 

## Invalidation

CloudFront caches can be invalidated using a wildcard. This forcibly clears the cache. Invalidations can be made manually via the AWS console or via the API. When deploying content that is sensitive to concerns like this, your process should include invalidation. For example, both the DUL Tool and the Event Data Artifact Manager tool include a call to the API to send an invalidation signal every time they make an update.

## DNS

Each CloudFront distribution is given a unique hostname. AWS handles the A records. DNS is done via CNAME to the hostname.

## TLS

CloudFront supports HTTPS, whilst it can serve HTTP requests, it's best to configure it to redirect protocols, as we should do everything we can to discourage the use of HTTP. You can upload your own certificate to the Certificate manager, or AWS will issue a gratis TLS certificate using its the AWS Root CA. 

The TLS termination uses SNI, which can cause problems for some HTTP clients, including off-the-shelf Java ones. There are standard workarounds, but this may generate support issues from users. 

## Terminating S3 Buckets

CloudFront can be used to serve S3 buckets over HTTPS, for example the Artifact Registry and the Evidence Registry in Event Data. This provides a very handy hybrid of object store and public read-oly REST API.

## CDN-Caching Application Fragments

CloudFront can be pointed to an arbitrary URL. This means it can be as a CDN for chunks of applications. We use this in Crossmark for caching the assets (images, CSS) but also the widget script. This gives a good trade-off between speedy serving of assets and versioned code assets. It's a little more complicated, see the documentation.

## Best Practice

Ensure certifiate expiry notifications include a CloudFront so that the AWS-stored certificates don't expire.

Ensure HTTPS endpoints are monitored as a last-ditch check for certificate expiry.

## Monitoring

CloudFront can be (and is) monitored by Pingdom. This can check not only the uptime of CloudFront (which should be extremely high) but also the availability of the Origin.

## External links

 - [Cache-Control](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control)
 - [SNI](https://en.wikipedia.org/wiki/Server_Name_Indication)
 - [AWS Certificate Manager](https://docs.aws.amazon.com/acm/latest/userguide/acm-overview.html)
 
