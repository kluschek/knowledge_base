---
area: distribution-querying
components: ''
datadeps:
- xml-query
- cddb
desc: Push XML data from the QS into the REST API.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-search
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- infra-datacenter-apache
- rest-api-cayenne
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Pusher (QS)
userfacing: false
---



Push XML data from the QS into the REST API.
