---
area: reporting-monitoring
components: ''
datadeps:
- auth-endpoint-v1
- crossmark-dialog
desc: Usage and deposit statistics for Crossmark. Allows members to log in and view
  dialog usage activity and deposit summaries.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- crossmark
related_services: []
repo_links:
- https://github.com/CrossRef/crossmark-statistics
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-vanilla
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossmark Statistics
userfacing: true
---



Usage and deposit statistics for Crossmark. Allows members to log in and view dialog usage activity and deposit summaries.
