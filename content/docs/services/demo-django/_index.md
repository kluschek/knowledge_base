---
area: tools-libraries
components: ''
datadeps: []
desc: Demo Django project with auto-generated API docs.
docs: []
lang: ''
lead: jwass
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/demo-django-project
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Demo Django Project
userfacing: false
---



Demo Django project with auto-generated API docs.
