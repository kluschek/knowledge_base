---
area: infrastructure-apps
components: ''
datadeps: []
desc: ''
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
- authorization-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Authentication API v1
userfacing: false
---



Content to follow
