---
area: reporting-monitoring
components: ''
datadeps:
- ext-doi
desc: Generate reports and send links to members by email.
docs: []
lang: Java
lead: jstark
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/reports/conflict.html
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/conflict_report
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Conflict report
userfacing: true
---
Generate reports and send links to members by email.
