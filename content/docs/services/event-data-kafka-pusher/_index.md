---
area: infrastructure-apps
components: ''
datadeps:
- event-data-percolator
desc: Listens on a Kafka Topic with Events and pushes to the Event Bus via HTTP.
docs: []
lang: ''
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_kafka_pusher
sentry_url: ''
servicedeps:
- infra-hetzner-docker
- infra-hetzner-kafka
- event-data-docker-base
- util-clojure
- infra-hetzner-elk
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Kafka Pusher
userfacing: false
---



Listens on a Kafka Topic with Events and pushes to the Event Bus via HTTP.
