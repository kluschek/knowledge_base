---
area: etl
components: ''
datadeps: []
desc: Periodic process to re-scan un-linked references to try to match using new data.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reference-linking
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Unresolved Reference System
userfacing: false
---



Periodic process to re-scan un-linked references to try to match using new data.
