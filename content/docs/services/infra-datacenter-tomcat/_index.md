---
area: infrastructure-services
components: ''
datadeps: []
desc: Service container used to run most Java services (deprecated)
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter-vanilla
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Tomcat (Data Center)
userfacing: false
---



Service container used to run most Java services (deprecated)
