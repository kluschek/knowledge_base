---
area: distribution-querying
components: ''
datadeps:
- event-data-event-bus
desc: Very simple live-stream of Event Data in the browser.
docs: []
lang: Clojure + JavaScript
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_live_demo
sentry_url: ''
servicedeps:
- event-data-docker-base
- infra-aws-kafka
- infra-aws-docker
- infra-aws-cloudwatch
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Live Demo
userfacing: true
---



Very simple live-stream of Event Data in the browser.
