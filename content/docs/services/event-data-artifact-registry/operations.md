---
related_services: []
repo_links: []
tags: []
title: Event Data Artifact Registry Operations / Maintenance
weight: 0
---





## Versioned, immutable files

Artifacts are versioned files. They define the behaviour of Event Data. The [official list of Artifacts](https://www.eventdata.crossref.org/guide/data/artifacts/) is in the user guide. Each Artifact is given a label and a version. Artifacts are immutable, which means that one a given version has been uploaded, it should never change. Instead, a new Artifact is uploaded. When Agents request an Artifact, they always request the latest version. It will record each version in the Evidence Record and Evidence Logs. 

## Updating Artifacts

Use the Artifact Manager to update Artifacts. This is currently quite a manual process.

 - Download a latest copy of the Artifact in question:
   - Visit <https://artifact.eventdata.crossref.org/a/artifacts.json>
   - Find the desired Artifact entry in the JSON.
   - Click on the `current-version-link`, which will be a URL like `https://artifact.eventdata.crossref.org/a/domain-decision-structure/versions/1536831537917`, to download the file
   - Edit the file.
   - Using the Artifact Manager command-line tool, upload the file. It will apply a datestamp and send invalidation signal to CloudFront.

