---
area: registries
components: ''
datadeps:
- event-data-artifact-manager
- human-direct-edit
desc: Artifacts used by Event Data Agents and Percolator.
docs:
- https://www.eventdata.crossref.org/guide/service/artifact-registry/
lang: JSON
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://artifact.eventdata.crossref.org
sentry_url: ''
servicedeps:
- infra-s3
- infra-cloudfront
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- web-domains
title: Event Data Artifact Registry
userfacing: true
---



Artifacts used by Event Data Agents and Percolator.
