---
area: infrastructure-services
components: ''
datadeps: []
desc: Kafka Queue system running in Hetzner. Used to transport data between services.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-hetzner
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Kafka (Hetzner)
userfacing: false
---



Kafka Queue system running in Hetzner. Used to transport data between services.
