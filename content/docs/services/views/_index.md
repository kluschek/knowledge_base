---
area: distribution-querying
components: ''
datadeps:
- cddb
desc: Present DOI metadata in various formats.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.view
prod_heartbeats: []
prod_urls: []
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-tomcat
- cddb
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DOI Metadata Views
userfacing: false
---




The following 'views' are available in the `org.crossref.qs.view` package. They aren't necessarily all used:

 - CitationSearchResultDebugView
 - CrossRefQueryOutputVersion2View
 - CrossmarkJsonView
 - CsvByTitleDepositView
 - CsvView
 - DelimitedDownloadView
 - DocumentView
 - DoiAndScoreView
 - DoiInfoView
 - DoiListView
 - ForwardLinkRenderer
 - ForwardLinkView
 - ForwardLinkView2
 - FuzzyTitleSearchView
 - HtmlByTitleDepositView
 - HtmlTableListView``
 - HtmlTableListViewWithFunders
 - IPageTemplateView
 - JsonView
 - JspHtmlView
 - JspTextView
 - JspXmlView
 - PipedLinkedView
 - PipedView
 - QSErrorView
 - QSErrorXmlView
 - QueriesXmlView
 - QuerySubmissionStatusView
 - RickshawView
 - StatusCodeView
 - SubmissionDownloadView
 - TitleAuthorAndScoreView
 - UnixmlView
 - UnixsdJsonView
 - UnixsdView
 - VerbatimTextView
 - VerbatimXmlView
 - ViewNames

