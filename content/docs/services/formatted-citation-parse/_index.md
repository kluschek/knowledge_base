---
area: etl
components: ''
datadeps: []
desc: Format citations into query objects. Part of the process of parsing references
  for matching.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.util.FormattedCitationParseService
- org.crossref.qs.controllers.FormattedCitationParseController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/fcparse/**
products:
- reference-linking
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- citation-search-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Formatted Citation Parse Service
userfacing: false
---




This represents both the `FormattedCitationParseService` and `FormattedCitationParseController` which wraps it.