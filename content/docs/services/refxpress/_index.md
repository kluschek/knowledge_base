---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.refXpressController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/refxpress/**
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- authorization-service
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags: []
title: RefXpress
userfacing: false
---




Content to follow.

