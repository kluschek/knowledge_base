---
area: websites
components: ''
datadeps:
- rest-api-cayenne
desc: Crossref site. Note that the website as browsed contains a mixture of Hugo-generated
  and web apps, routed by HAProxy.
docs: []
lang: Hugo
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/cr-site
sentry_url: ''
servicedeps:
- assets-cdn
- rest-api-cayenne
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Site
userfacing: true
---



Crossref site. Note that the website as browsed contains a mixture of Hugo-generated and web apps, routed by HAProxy.
