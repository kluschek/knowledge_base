---
area: reporting-monitoring
components: ''
datadeps:
- event-data-evidence-log
- event-data-artifact-registry
- event-data-evidence-registry
desc: Service for continual quality assurance and automatic quality issues investigation
docs: []
lang: ''
lead: jwass
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_investigator
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Investigator
userfacing: false
---



Service for continual quality assurance and automatic quality issues investigation
