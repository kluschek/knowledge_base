---
related_services: []
repo_links: []
tags: []
title: Operations of Member Info
weight: 0
---



This document details the operation of the Member Info service (MIS).

## Running

The Docker image is registered as

    crossref/memberinfo-list
    
To run the image

    docker run --env-file=billerica.env crossref/memberinfo-list
    
where the billerica.env file contains the environment variable values suitable for 
use in the Billerica data-center.

## Status

The status of MIS can be checked in the logs. When MIS starts an update it will log

    running memberinfo-list update

For example, Spring Boot's standard logger will output

    2019-12-11 11:28:56.850 INFO 98262 --- [ main] o.c.memberinfo.MemberinfoApplication : running memberinfo-list update

If the update succeeds then MIS will log

    ran memberinfo-list update

Eg

    2019-12-11 11:28:56.850 INFO 98262 --- [ main] o.c.memberinfo.MemberinfoApplication : ran memberinfo-list update
    
If the update fails then MIS will log

    unable to complete memberinfo-list update

Eg

    2019-12-11 11:28:56.850 ERROR 98262 --- [ main] o.c.memberinfo.MemberinfoApplication : unable to complete memberinfo-list update

If the MIS is daemonized for scheduled updates then at startup the MIS will log

    scheduling updates: cron-expression=

## Environment Variables

MIS draws its data from the Content System, the Customer Db, Sugar, and 
Intacct. It needs to write its data to Suagr and to AWS S3. It thus needs 
access control details for each of these service.

### Runtime

MIS can be run for a single update or daemonized for scheduled updates. 
For scheduled updates provide the environment varaiable

    application_schedule=«cron expression»

Where cron-expression consists of the 6 fields

    second minute hour day-of-month month day-of-week
    
(Note the addition of seconds to the standard cron fields.) 
See [man 5 crontab](https://linux.die.net/man/5/crontab) for details.

### Content System

MIS needs read-only access to the Content System's deposit database (aka Oracle). 
Provide the access details in the envirionment variables

    deposit_url=«jdbc url»
    deposit_user=«database account name»
    deposit_password=«database account password»

### Customer Db

MIS needs to read-write access to the Customer Db. Provide the access details in 
the envirionment variables

    customer_url=«jdbc url»
    customer_user=«database account name»
    customer_password=«database account password»

### Sugar

MIS needs read-write access to Sugar API. Provide the access details in the 
envirionment variables

    sugar_user=
    sugar_password=

### Intacct

MIS needs read-only access to Intacct API. Provide the access details in the 
envirionment variables

    intacct_sender_user=
    intacct_sender_password=

    intacct_login_user=
    intacct_login_password=
    intacct_login_company=

### AWS S3

MIS needs read-write access to an AWS S3 bucket. Provide the access details, 
region, and bucket name in the envirionment variables

    s3_bucket_accesskey=«AWS access key»
    s3_bucket_secretkey=«AWS access key»
    s3_bucket_region=«AWS region name»
    s3_bucket_name=«bucket name»
    application_id=«instance name»
   
The S3 bucket must exist. 

The application id is used in the bucket item name to identify the MIS instance 
that created the item. Keep the application_id short, perhaps in the convention 
used by CS deployments, eg "ms-1".

