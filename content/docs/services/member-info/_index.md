---
area: infrastructure-apps
components: ''
datadeps:
- ext-sugar
- ext-intacct
desc: Internal process to ingest data and distribute it internally.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.PutController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/internal/put/memberinfoupdates/**
products:
- metadata-manager
- metadata-plus
- metadata-retrieval
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/member_info
sentry_url: ''
servicedeps:
- infra-datacenter-tomcat
- infra-s3
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- member-info
title: Member Info System
userfacing: false
---




Internal process to ingest data from internal and external sources, package, and distribute internally.  

Data saved in location specified by `qs.member-info.update.servant.directory`.