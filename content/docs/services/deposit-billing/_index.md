---
area: reporting-monitoring
components: ''
datadeps:
- deposit-events
desc: Generate quarterly billing files for upload to Intacct system.
docs: []
lang: ''
lead: jstark
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Deposit Billing
userfacing: false
---



Generate quarterly billing files for upload to Intacct system.
