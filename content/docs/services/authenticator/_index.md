---
area: infrastructure-apps
components: community-data
datadeps: []
desc: Authenticator service for authenticating Crossref members. Work in progress.
docs: []
lang: Python / Django
lead: japaro
legacy: false
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-manager
- metadata-plus
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/authenticator
sentry_url: ''
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-docker
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- auth
title: Authenticator
userfacing: true
---

Authenticator service for authenticating Crossref members. Current functionality includes:
 
 - Storage of securely hashed passwords.
 - API for checking credentials, used by the Community Data service.
 - Password reset flow.
