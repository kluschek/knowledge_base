---
area: reporting-monitoring
components: ''
datadeps:
- event-data-event-bus
- event-data-percolator
- event-data-agents
desc: Log of actions taken by Agents, Percolator, Quality. Stream of data, snapshotted.
docs:
- https://www.eventdata.crossref.org/guide/service/evidence-logs/
lang: JSON
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_investigator
sentry_url: ''
servicedeps:
- event-data-query-api
- infra-s3
- ext-twitter
- event-data-artifact-registry
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Evidence Log
userfacing: false
---




Evidence Logs are produced by Agents and Percolator. They are written to a Kafka topic. These logs are structured and intended for public consumption. For more information see the User Guide. The are different to Application logs because:

1. Every entry is structured JSON with defined fields.
2. Every log type is documented in the User Guide.
3. They contain only public log messages that will be of interest to users trying to understand the data.

These are made available to the public via hourly snapshots. 

Historically this feature was known as 'status', so you may see references to this in the source code or configuration parameters.

## Monitoring

The Evidence Log plays two roles in monitoring, both via [Heartbeats](/eventdata/application/heartbeats): 

1. The logs are monitored by heartbeats. The Evidence Log shows live activity, so, for example, if an Agent stops working this will be detected. 
2. The logs are snapshotted on an hourly (previously daily) basis [See: Periodic Tasks](/eventdata/application/periodic). These are checked by heartbeats URL checks. If a snapshot is missed, it will generate an Error via the Quality heartbeat.
