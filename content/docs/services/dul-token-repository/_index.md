---
area: registries
components: ''
datadeps:
- dul-authority-tool
desc: Registry of DUL Providers and their Public Keys.
docs: []
lang: JSON + JWS
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- distributed-usage-logging
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-s3
- infra-cloudfront
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Distributed Usage Logging (DUL) Registry
userfacing: true
---



Registry of DUL Providers and their Public Keys.
