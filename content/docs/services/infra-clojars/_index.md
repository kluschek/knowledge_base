---
area: infrastructure-services
components: ''
datadeps: []
desc: Storage of Clojure library artifacts.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.clojars.org
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Clojars Repository
userfacing: false
---



Storage of Clojure library artifacts.
