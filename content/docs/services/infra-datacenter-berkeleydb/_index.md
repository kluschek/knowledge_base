---
area: infrastructure-services
components: ''
datadeps: []
desc: BerkeleyDB running in data center. This is an in-process library, not a distinct
  serivce. However, wherever it is used it requires persistent disk storage.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: BerkeleyDB
userfacing: false
---



BerkeleyDB running in data center. This is an in-process library, not a distinct serivce. However, wherever it is used it requires persistent disk storage.
