---
area: distribution-querying
components: ''
datadeps: []
desc: List of funder names.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- /WEB-INF/fundref/fundrefFundersList.jsp
- org.crossref.qs.fundref.FundRefDao
- org.crossref.qs.fundref.FundRefFunderListController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/funderNames
- https://apps.crossref.org/funderNames
products:
- funder-registry
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- funder-registry
title: Funder Names List
userfacing: false
---




Content to follow.

