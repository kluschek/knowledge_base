---
area: external
components: ''
datadeps:
- deposit-processor
desc: DOI.org / Handle Server . Resolve DOIs, redirect for content negotiation, serve
  up DUL headers.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://hdl.handle.net
- doi.org
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- content-negotiation
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DOI Handle Server
userfacing: false
---



DOI.org Handle Server . Resolve DOIs, redirect for content negotiation, serve up DUL headers.
