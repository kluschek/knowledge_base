---
area: metatdata-deposit
components: ''
datadeps:
- cddb
desc: ''
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- webDeposit
prod_heartbeats: []
prod_urls:
- https://apps.crossref.org/webDeposit/
- https://www.crossref.org/webDeposit/**
products:
- metadata-deposit
related_services: []
repo_links:
- https://gitlab.com/crossref/web_deposit
sentry_url: ''
servicedeps:
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- web-deposit
title: Web Deposit
userfacing: true
---


webDeposit is a legacy tool to allow end users to deposit data via a step-by-step form entry system. It supports journals, book, conferences, reports, dissertations, crossmark policy pages, NLM (JATS) files, csv resource files.
