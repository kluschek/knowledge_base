---
area: registries
components: ''
datadeps:
- open-funder-registry
desc: Registry of Funders maintained in GitHub for versioning.
docs: []
lang: XML
lead: myalter, jhanna
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- funder-registry
related_services: []
repo_links:
- https://github.com/CrossRef/open-funder-registry
sentry_url: ''
servicedeps:
- open-funder-registry
- infra-github
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Open Funder Registry (GitHub)
userfacing: true
---



Registry of Funders maintained in GitHub for versioning.
