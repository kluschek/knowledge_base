---
area: reporting-monitoring
components: ''
datadeps:
- deposit-processor
desc: Store Deposit activity of members for billing.
docs: []
lang: ''
lead: jstark
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags:
- billing
title: Deposit Events Table
userfacing: false
---



Store Deposit activity of members for billing.
