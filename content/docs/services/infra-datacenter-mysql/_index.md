---
area: infrastructure-services
components: ''
datadeps: []
desc: MySQL database running in data center.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-datacenter
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: MySQL Datacenter
userfacing: false
---



MySQL database running in data center.
