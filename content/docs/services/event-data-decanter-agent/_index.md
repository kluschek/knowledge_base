---
area: collection
components: ''
datadeps:
- oai-pmh
desc: Collection of Data Citations from Crossref
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- event-data-event-bus
- infra-hetzner-docker
- oai-pmh
- infra-aws-cloudwatch
- infra-hetzner-elk
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Decanter Data Citation
userfacing: false
---



Collection of Data Citations from Crossref
