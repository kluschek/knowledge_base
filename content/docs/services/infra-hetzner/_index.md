---
area: infrastructure-services
components: ''
datadeps: []
desc: Hetzner Cloud host.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.hetzner.de
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Hetzner host
userfacing: false
---




Hetzner Cloud is fully automatable, and every feature we use is fully automated. The product offering is far simpler than AWS, both in terms of diversity (they offer virtual machines only) and detail (there is no configurable virtual networking or machine attributes). Hetzner cloud is significantly cheaper than AWS, but offers a more basic product. We use Hetzner Cloud for the Event Data Backend, which includes Agents that collect data, both asynchronously and synchronously. We don't host any user-facing services on Hetnzer Cloud.

Hetzner also offer a bare-metal service, which we use for labs projects but currently not for production services.


# Locking VMs

Once most virtual machines are set up, they probably won't be intentionaly changed. 

As a last backstop, VMs can be manually locked in the console. In most cases, once you create a virtual machine, there's a high probability that you won't want to modify it or delete it. It therefore makes sense to log in and manually lock each virtual machine (by clicking the padlock). 

The only downside to this is that it takes a few more seconds to delete a virtual machine.

