---
area: distribution-querying
components: ''
datadeps:
- rest-api-cayenne
desc: Crossref search interface
docs: []
lang: Ruby
lead: edatta
legacy: true
owner: kmeddings
packages: []
prod_heartbeats:
- https://search.crossref.org/heartbeat
prod_urls:
- https://search.crossref.org
products:
- metadata-retrieval
- metadata-search
related_services: []
repo_links:
- https://github.com/CrossRef/cr-search
sentry_url: ''
servicedeps:
- rest-api-cayenne
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://search.staging-legacy.crossref.org
tags: []
title: Crossref Metadata Search & Funder Search
userfacing: false
---



Crossref search interface
