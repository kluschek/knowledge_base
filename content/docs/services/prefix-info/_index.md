---
area: distribution-querying
components: ''
datadeps: []
desc: Return Prefixes associated with the authenticated user.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.InfoController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/info
- https://doi.crossref.org/info?rtype=prefixes
products:
- crossmark
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- authorization-service
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- crossmark
- authenticated
title: Prefix Info
userfacing: true
---




## Parametesr

 - `usr`
 - `pwd`
 - `rtype` must have the value `prefixes`
 
## Prefixes

List of prefixes and Crossmark Prefixes assigned to the username. 