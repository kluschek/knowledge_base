---
area: greenfield
components: metadata-repository
datadeps:
- metadata-bucket
desc: A repository containing tools to manage metadata updates
docs: []
lang: ''
lead: ''
legacy: false
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services:
- metadata-bucket
repo_links: []
sentry_url: ''
servicedeps:
- metadata-bucket
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- metadata
title: Metadata Bucket Updates
userfacing: false
---

Metadata Bucket Updates is a [repository](https://gitlab.com/crossref/metadata_bucket_updates) that contains a number of tools for managing updates to metadata.


## Metadata Bucket Builder

Metadata Bucket Builder can build an initial metadata bucket using a snapshot as source. It will build the bucket in accordance with the spec defined by the [metadata bucket](../metadata-bucket).

It is built using Python. Python was used here because it is a good choice to run an ad-hoc script like `bucket_builder/build_bucket.py` and because it
has good support in AWS Lambda, which will be used for other metadata update tools.


A command to build the bucket might look something like this:

```
AWS_PROFILE=crossref-staging python bucket_builder/build_bucket.py --snapshot_path /path/to/extracted/snapshot --destination_bucket=crossref-metadata-bucket-temp
```

Or, if you don't want to build to S3, you can build to a local directory

```
python bucket_builder/build_bucket.py --snapshot_path /path/to/extracted/snapshot --destination_directory /home/my-user/some-dir
```

## Metadata Bucket Subscriber

Metadata Bucket Subscriber subscribes to changes to data in the metadata bucket and pushes the keys related to those changes into a Kafka topic.  

Messages will be pushed to the following topics:

- metadata_bucket_updates_xml
- metadata_bucket_updates_citedbycount

These Kafka topics are used to notify the REST API that it should retrieve an object from the bucket and update its own indexes.
