---
area: infrastructure-services
components: ''
datadeps: []
desc: Amazon Web Services.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: AWS
userfacing: false
---



Amazon Web Services.
