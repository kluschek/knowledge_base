---
area: distribution-querying
components: ''
datadeps:
- cddb
- member-info
desc: Query and retrieve archives of metadata. Plus members only.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-plus
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: OAI-PMH Service
userfacing: true
---



Query and retrieve archives of metadata. Plus members only.
