---
area: tools-libraries
components: ''
datadeps:
- human-direct-edit
desc: Tool for updating the DUL Registry and uploading Public Keys
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- distributed-usage-logging
related_services: []
repo_links:
- https://github.com/CrossRef/dul-authority-tool
sentry_url: ''
servicedeps:
- infra-s3
- infra-cloudfront
- dul-token-repository
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DUL Authority Tool
userfacing: false
---



Tool for updating the DUL Registry and uploading Public Keys
