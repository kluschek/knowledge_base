---
area: external
components: ''
datadeps: []
desc: Twitter is an Internet service that allows users to post one-line summaries
  of their current state, and to view the current state of other users. It fulfills
  a niche somewhere between blogs and customisable status texts or display names.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.twitter.com
products:
- event-data
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Twitter API
userfacing: false
---



Twitter is an Internet service that allows users to post one-line summaries of their current state, and to view the current state of other users. It fulfills a niche somewhere between blogs and customisable status texts or display names.
