---
area: registries
components: ''
datadeps:
- event-data-evidence-log-snapshot
- event-data-percolator
- event-data-agents
desc: Evidence Files produced by the Agents, Percolator etc.
docs:
- https://www.eventdata.crossref.org/guide/service/evidence-registry/
lang: JSON
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://evidence.eventdata.crossref.org
sentry_url: ''
servicedeps:
- infra-s3
- infra-cloudfront
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Evidence Registry
userfacing: true
---



Evidence Files produced by the Agents, Percolator etc.
