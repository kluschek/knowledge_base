---
area: reporting-monitoring
components: ''
datadeps:
- event-data-evidence-log
desc: Echo Evidence Log to application logs for monitoring.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-evidence-logger
sentry_url: ''
servicedeps:
- infra-aws-kafka
- infra-aws-cloudwatch
- event-data-evidence-log
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Evidence Logger
userfacing: false
---



Echo Evidence Log to application logs for monitoring.
