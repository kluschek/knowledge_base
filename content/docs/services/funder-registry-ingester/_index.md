---
area: tools-libraries
components: ''
datadeps:
- ext-funder-skos
desc: Tool in the Admin Dashboard to check and upload Funder Registry data.
docs: []
lang: Java
lead: jhannna
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Funder Registry Ingester
userfacing: false
---



Tool in the Admin Dashboard to check and upload Funder Registry data.
