---
area: registries
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/schemas/
- https://data.crossref.org/depositSchema/
- https://apps.crossref.org/schemas
- https://data.crossref.org/depositSchema
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Schemas Pages
userfacing: true
---

Hosted Crossref schema pages. Note that the `data.crossref.org` version results in an infinite redirect, however it is still served and present in HAProxy.
