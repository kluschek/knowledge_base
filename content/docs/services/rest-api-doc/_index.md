---
area: documentation
components: ''
datadeps: []
desc: Documentation for Crossref's REST API.
docs: []
lang: ''
lead: ppolischuk
legacy: true
owner: ppolischuk
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-retrieval
related_services: []
repo_links:
- https://github.com/CrossRef/rest-api-doc
sentry_url: ''
servicedeps:
- infra-github
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: REST API Doc (Deprecated)
userfacing: true
---



Documentation for Crossref's REST API.
