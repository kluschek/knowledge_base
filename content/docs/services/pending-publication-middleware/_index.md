---
area: distribution-querying
components: ''
datadeps: []
desc: Middleware for Pending Publications
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.PendingPubMidwareController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/search/pending/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- doi-citation-search
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- pending-publications
title: Pending Publication Middleware
userfacing: false
---




## Notes

Has an implicit dependency on DOI Citation Search via an HTTP call using password supplied by properties in the Spring Context bean.



