---
area: distribution-querying
components: ''
datadeps:
- rest-api-cayenne
desc: Serve up snapshots and diffs to JSON Metadata in the ResourceSync format.
docs: []
lang: ''
lead: ''
legacy: true
owner: gbilder
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- rest-api-cayenne
- infra-s3
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: ResourceSync (Proposed)
userfacing: false
---



Serve up snapshots and diffs to JSON Metadata in the ResourceSync format.
