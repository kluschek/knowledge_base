---
area: infrastructure-apps
components: ''
datadeps: []
desc: Storage of files. Used to service user-facing requests such as notifications
  and funder registry.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- funder-registry
- metadata-retrieval
- multiple-resolution
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Interim Storage System
userfacing: true
---



Storage of files. Used to service user-facing requests such as notifications and funder registry.
