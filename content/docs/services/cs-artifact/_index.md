---
area: infrastructure-apps
components: ''
datadeps: []
desc: Keeping track of activities that occur so we can monitor the behaviour of the
  system.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-plus
- metadata-retrieval
- metadata-search
- orcid-auto-update
- reference-linking
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags: []
title: CS Artifact System
userfacing: false
---



Keeping track of activities that occur so we can monitor the behaviour of the system.
