---
related_services:
- crossref-schemas
repo_links:
- https://gitlab.com/crossref/content_system
tags:
- clinical-trial-numbers
title: Clinical Trial Number Processing
weight: 0
---

CTNs may be present in the following Content types:
 - Journal Article 
 - Book Chapters
 - Conference Papers
 - Stand Alone (clinical trial data)

 
Linked Clinical Trial Numbers are processed either as part of a metadata deposit, such as Journal articles, and must have
a relationship with an existing DOI. Alternativly, a stand alone clinical trial deposit can be made using the 
clinicaltrial_data element. 

Element structure example: >
    <ct:clinical-trial-number registry="10.18810/isrctn" type="results">ISRCTN1234</ct:clinical-trial-number>

There are three elements that make up the structure of the LCT metadata:

    - The registry id (required)
        This is registry in which the clinical trial has been registered. Clinical trials should be registered with one of 
        the WHO-approved national trial registries (or Clinicaltrials.gov). Crossref maintains a list of these approved 
        registries and has assigned a DOI to each one. The registry id is used in combination with the trial number 
        to identify trials correctly.
        
    - The registered clinical trial number (required)
        This must include its prefix, example: ISRCTN1234
        
    - The relationship of the publication to the clinical trial (optional)
        This field is optional but encouraged. The three allowed elements are “pre-results”, 
        “results” and “post-results”. They indicate which stage of the trial the publication is reporting on.

Metadata (e.g. Journal Articles) and Stand Alone Deposits: |

Submitted linked clincial trial metadata is validated initially by the schema (clinicaltrials.xsd). The registered clinical 
trial number are checked against a regional regular expression pattern within the registry.xml (http://api.crossref.org/works/10.18810
/registries.xml) to determine LCT numbers validity. 

Withing the code base, the Clinical trial number is validated again against the LCT registry through the associated DOI. If the 
record is found and the relations contains the 10.18810/registries. Then, the clinical number will be extracted and formated 
appropriately. The record (HAS_LCT), will then be stored in the (JOURNAL_ARTICLE_TAG_SUITE) table (or deleted if LCT number is 
removed) and the clob data(uniclob_info) will be updated.  

The stand alone LCT deposit goes through a similar process as the Journal Article (but processed through the DOIResourceSE). Where the DOI relation is verified and 
the registered clinical trial number is validated.  

Stand alone structure example: |

    ```
    <clinicaltrial_data>
    <doi>10.5555/12345678</doi>
    <ct:program>
    <ct:clinical-trial-number registry="10.18810/isrctn" type="results">ISRCTN1234</ct:clinical-trial-number>
    <ct:clinical-trial-number registry="10.18810/isrctn" type="results">ISRCTN9999</ct:clinical-trial-number>
    </ct:program>
    </clinicaltrial_data>
    ```

