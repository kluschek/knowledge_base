---
area: ingestion
components: ''
datadeps:
- xml-deposit-synchronous-2
- xml-deposit-api
- infra-datacenter-activemq
- jats-xslt
desc: Process deposits based on content type.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- deposit-queue
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- jats
title: Deposit Processor
userfacing: false
---



Process deposits based on content type.
