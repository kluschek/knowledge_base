---
area: tools-libraries
components: ''
datadeps:
- crossmark-dialog
desc: A tool and library offered to our members, without support, to help them prepare
  Crossmark-stamped PDFs. Stamp a PDF with an image and clickable URL. Informal relationship
  with Crossmark.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/pdfstamp
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: PDF Stamp
userfacing: true
---



A tool and library offered to our members, without support, to help them prepare Crossmark-stamped PDFs. Stamp a PDF with an image and clickable URL. Informal relationship with Crossmark.
