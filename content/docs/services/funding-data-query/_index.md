---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: Java
lead: ''
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls: []
products:
- funder-registry
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Funding Data Query System
userfacing: true
---



Content to follow
