---
area: infrastructure-services
components: ''
datadeps: []
desc: ZooKeeper distributed configuration service
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services:
- infra-aws-kafka
repo_links: []
sentry_url: ''
servicedeps:
- infra-aws
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: ZooKeeper (AWS)
userfacing: false
---




Supports Kafka.