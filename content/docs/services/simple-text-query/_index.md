---
area: distribution-querying
components: ''
datadeps:
- cddb
desc: ''
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.simpletextquery.SimpleTextQueryService
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/SimpleTextQuery/**
- https://doi.crossref.org/SimpleTextQuery
- https://doi.crossref.org/simpleTextQuery/**
- https://doi.crossref.org/simpleTextQuery
products:
- metadata-search
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-tomcat
- cited-by-deposit-service
- authorization-service
- cs-artifact
- pubmed-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- simple-text-query
title: Simple Text Query System (STQ)
userfacing: true
---



Content to follow
