---
area: reporting-monitoring
components: ''
datadeps:
- event-data-agents
- event-data-percolator
- event-data-artifact-registry
- event-data-evidence-log
desc: Monitor activity of Agents and Percolator and compare to rules stipulated in
  an Artifact. This is monitored by Pingdom. Alarms will sound if Agents, Percolator
  or Snapshots fall below expected.
docs: []
lang: ''
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-heartbeat
sentry_url: ''
servicedeps:
- infra-aws-kafka
- infra-aws-docker
- infra-aws-cloudwatch
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Quality & Heartbeat
userfacing: false
---



Monitor activity of Agents and Percolator and compare to rules stipulated in an Artifact. This is monitored by Pingdom. Alarms will sound if Agents, Percolator or Snapshots fall below expected.
