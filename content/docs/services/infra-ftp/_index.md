---
area: infrastructure-services
components: ''
datadeps: []
desc: FTP Server (over HTTP)
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://ftp.crossref.org
- https://data.crossref.org/
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: ftp.crossref.org
userfacing: false
---

AKA VFTP

Server formerly FTP, now uses HTTP