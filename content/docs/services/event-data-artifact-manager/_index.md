---
area: tools-libraries
components: ''
datadeps:
- human-direct-edit
desc: Tool for managing the Artifact Registry for Event Data.
docs: []
lang: ''
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-artifact-manager
sentry_url: ''
servicedeps:
- event-data-common
- event-data-artifact-registry
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Artifact Manager
userfacing: false
---



Tool for managing the Artifact Registry for Event Data.
