---
area: ingestion
components: ''
datadeps:
- auth-endpoint-v1
- xml-query
- rest-api-cayenne
desc: Metadata deposit tool user interface. Saves state and submits to synchronous
  deposit API.
docs: []
lang: Java
lead: myalter, jhanna
legacy: true
owner: ppolischuk
packages: []
prod_heartbeats:
- https://apps.crossref.org/mdt/v1/heartbeat
prod_urls:
- https://apps.crossref.org/mdt/v1
products:
- metadata-manager
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- xml-deposit-synchronous-2
- infra-datacenter-tomcat
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats:
- apps-staging.crossref.org/mdt/heartbeat
staging_urls:
- https://apps.crossref.org/mdt/v1
tags: []
title: Metadata Manager Middleware
userfacing: false
---



Metadata deposit tool user interface. Saves state and submits to synchronous deposit API.
