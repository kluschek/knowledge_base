---
area: greenfield
components: metadata-repository
datadeps:
- cddb
- members-service
desc: Tool to push XML clob/unixsd to S3 bucket
docs: []
lang: Java
lead: ''
legacy: false
owner: ''
packages:
- org.crossref.qs.crmds.S3ClobPusher
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: ''
servicedeps:
- metadata-bucket
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: XML S3 Bucket Pusher
userfacing: false
---

## Running the tool 

The tool needs to be built with the standard `ant compile` or `./bin/nbant compile` on developer machine. 
It needs a default deployment property, and is set to use the developer myalter-1 property file (may need to revisit)

From standard developer environment, use the command line:
`bin/j  --cp ./java/org/crossref/qs/crmds/ org.crossref.qs.crmds.S3ClobPusher` to run the tool using all the defaults, for the entire DOI corpus. 

Parameter options:
```
--endId # [default=0]
--startId # [default=500000000]
--awsSecretKey AWS_KEY
--awsAccessKey AWS_ACCESS_KEY
--help
```

Parameters can be supplied immediately after the class to run:
`bin/j  --cp ./java/org/crossref/qs/crmds/ org.crossref.qs.crmds.S3ClobPusher --startId 0 --endId 1000`
