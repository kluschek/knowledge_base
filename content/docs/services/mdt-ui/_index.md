---
area: ingestion
components: ''
datadeps:
- mdt-middleware
desc: Metadata deposit tool user interface. Note that the middleware has further dependenciesxx.
docs: []
lang: JavaScript
lead: myalter, jhanna
legacy: true
owner: ppolischuk
packages: []
prod_heartbeats:
- https://www.crossref.org/mmstaging/
prod_urls:
- https://www.crossref.org/metadatamanager/
products:
- metadata-manager
related_services: []
repo_links:
- https://github.com/CrossRef/mdt-ui
sentry_url: ''
servicedeps:
- mdt-middleware
- infra-datacenter-apache
- assets-cdn
- auth-endpoint-v1
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Metadata Manager UI
userfacing: true
---



Metadata deposit tool user interface. Note that the middleware has further dependenciesxx.
