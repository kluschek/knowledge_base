---
area: documentation
components: ''
datadeps: []
desc: User-facing documentation for Event Data.
docs: []
lang: ''
lead: jwass
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-user-guide
sentry_url: ''
servicedeps:
- infra-s3
- infra-cloudfront
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data User Guide
userfacing: true
---



User-facing documentation for Event Data.
