---
area: etl
components: ''
datadeps:
- search-based-reference-matcher
- formatted-citation-parse
desc: Reference Matching within Deposit Processing
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- reference-linking
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Reference Matching
userfacing: false
---



Reference Matching within Deposit Processing
