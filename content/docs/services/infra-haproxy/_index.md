---
area: infrastructure-services
components: ''
datadeps:
- member-info
desc: HAProxy does everything, so it's not all shown here. Takes CIDR tables from
  MemberInfo (?).
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/haproxy-conf
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- member-info
title: HAProxy
userfacing: false
---




HAProxy is responsible for tasks including:

 - handling all incoming traffic to the Data Center
 - terminating TLS certificates
 - internal load-balancing
 - blocking users 
