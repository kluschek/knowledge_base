---
related_services:
- metadata-bucket-updates
- metadata-bucket
repo_links: []
tags:
- metadata
title: Ingesting metadata from S3
weight: 0
---

The REST API can be run in a profile that will cause it to ingest metadata from S3. An example of this can be seen below.

```
AWS_PROFILE=crossref-staging AWS_REGION=eu-west-1 METADATA_BUCKET=crossref-metadata-bucket-temp lein run :nrepl :api :s3-ingest
```

When run with `s3-ingest` the rest API will start a task that will page through all data in `METADATA_BUCKET` and index the data in Elasticsearch.

It is possible to index a subset of data in `METADATA_BUCKET` by using `METADATA_PREFIX`, here you can specify a [prefix](https://docs.aws.amazon.com/general/latest/gr/glos-chap.html#keyprefix), for example:

```
AWS_PROFILE=crossref-staging AWS_REGION=eu-west-1 METADATA_BUCKET=crossref-metadata-bucket-temp METADATA_SEARCH_PREFIX=10.1145/253228.253255 lein run :nrepl :api :s3-ingest
```

If you do not wish to rely on S3 then you can use a local directory, like so:

```
METADATA_BUCKET=/location/to/local/metadata METADATA_LOCAL_STORAGE=1 lein run :nrepl :api :s3-ingest
```

**Note** `METADATA_BUCKET` must have been built using [Metadata Bucket Builder](../../metadata-bucket-updates/#metadata-bucket-builder) or another tool that conforms to [the spec](../../metadata-bucket/).
