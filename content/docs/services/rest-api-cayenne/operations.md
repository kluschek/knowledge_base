---
related_services: []
repo_links: []
tags:
- blocking
title: REST API Operations
weight: 0
---



## Blocking Users

If users are causing the REST API it may be necessary to block their IP address as a last resort. See [HAProxy operations]({{< relref "/docs/services/infra-haproxy/blocking-users" >}}).

## Elastic Search

The number of open cursor sessions can be retrieved at `/health/cursors`. This may be useful for monitoring the scalability of cursors.

## Startup tasks

### :create-mappings

Creates ES indexes, if they do not already exist. Defined in `cayenne.elastic.mappings`. The following properties are set for each index:

* number of shards: configuration parameter `[:service :elastic :shard-count]`
* number of replicas: configuration parameter `[:service :elastic :replica-count]`
* max result window: configuration parameter `[:service :elastic :result-size-window-works]` (for `/works`) and `[:service :elastic :result-size-window]` (for all remaining indexes) 

### :process-feed-files

### :update-members

Indexing members and calculating coverage for members. Schedule in `cayenne.schedule`.

See also https://gitlab.com/crossref/knowledge_base/wikis/Data-Flows/Member-Info

### :update-journals

Indexing journals and calculating coverage for journals. Schedule in `cayenne.schedule`.

See also https://gitlab.com/crossref/knowledge_base/wikis/Data-Flows/Journal-Info

### :update-funders

### :update-subjects

Indexing Scopus journal subjects and adding the subjects to journal records. Schedule in `cayenne.schedule`.

Note that this task should start after journals are indexed.

See also https://gitlab.com/crossref/knowledge_base/wikis/Data-Flows/Journal-Subjects

### :cleanup-feed-files

Schedules quartz runner jobs that take care of deleting old processed feed files from disc. In the current configuration, once every hour files older than 2 hours are deleted from `/feed-processed` directory. The time and delay can be changed in `cayenne.schedule` namespace.

### :api

### :nrepl