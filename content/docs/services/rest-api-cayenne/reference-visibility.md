---
related_services: []
repo_links: []
tags:
- configuration
- json
title: Reference visibility in REST API
weight: 0
---

When the REST API is run with appropriate configurations it is able to return a combination of `open`, `limited`, and `closed` references in its json response. If no configuration is supplied then by default the rest api will only return `open` references.

This functionality can be configured with the environment variable `REFERENCES`, and it can take one of the following values:

1. `open` - only open references are returned
2. `limited` - open and limited references are returned
3. `closed` - open, limited, and closed references are returned
