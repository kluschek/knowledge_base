---
related_services: []
repo_links: []
tags:
- subject-codes
title: SCOPUS journal subjects ingestion in REST API
weight: 0
---



### ASJC classification codes

ASJC classification codes are ingested from a location configured as `[:location :scopus-title-list]`. It points to a XLSX file made available by Elsevier, which contains: 1) mapping ASJC codes to names, and 2) mapping journal ISSNs to ASJC codes. ASJC codes and names are read from this file and indexed in a `subject` ES index.

{{< mermaid >}}
graph LR;
  elsevier(www.elsevier.com);
  ingest(Cayenne Task Ingest Subjects);
  index(Cayenne ES Subjects Index);
  elsevier-->|XLSX file|ingest;
  ingest-->index;
{{< /mermaid >}}

### Updating journal subjects

The same XLSX file is also used to insert journal subjects into journal records in `journal` ES index. A separate function reads mappings ASJC code - ISSN from XLSX file, and uses data from previously populated `subject` ES index to insert the ASJC code and name into the journal record.

{{< mermaid >}}
graph LR;
  elsevier(www.elsevier.com);
  ingest(Cayenne Task Update Journals Subjects);
  indexsubj(Cayenne ES Subjects Index);
  indexjourn(ES Journals Index);
  api(Cayenne /v1/journals);
  elsevier-->|XLSX file|ingest;
  indexsubj-->|ASJC name lookup|ingest;
  ingest-->indexjourn;
  indexjourn-->api;
{{< /mermaid >}}


### Inserting journal subjects into works

During works ingestion, journal subjects are attached to works metadata based on journal ISSN. See https://gitlab.com/crossref/knowledge_base/wikis/Data-Flows/Works