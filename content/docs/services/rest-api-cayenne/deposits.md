---
related_services: []
repo_links: []
tags:
- deposits
- mongo
title: Deposit handoff in REST API
weight: 0
---

When the REST API handles a `application/vnd.crossref.deposit+xml` deposit created via a POST to `/deposits` the following process is carried out:

1. `create-deposit!` - Creates a new deposit in MongoDB with a `handoff.status` of `incomplete`
2. `begin-handoff!` - Begins the handoff to doi.crossref.org, this doesn't impact `handoff.status` but is used to track failure and retry count
3. `perform-xml-deposit-handoff` - Carries out the deposit using doi.crossref.org

If the deposit to doi.crossref.org has failed:

4. `failed!`- Marks the handoff as complete and updates `status` to `failed`, *note* `status` not `handoff.status`. `status` indicates the deposit actually failed, not that there was an issue with the handover that can be retried

If the deposit to doi.crossref.org is successful:

4. `end-handoff!`- Marks the handoff as complete and updates `handoff.status` to `completed`
5. https://gitlab.com/crossref/deposit_update_message_handler monitors the mailbox associated with deposits made via the REST API, if there are any errors reported then it updates the deposit `status` to `failed`, otherwise to `completed`



