---
area: distribution-querying
components: query-content-negotiation
datadeps:
- cs-pusher
- open-funder-registry
- cited-by-count-query
- title-file
- scopus-title-list
desc: API for querying and retrieving data in JSON format. This serves most endpoints
  on api.crossref.org, but not all.
docs: []
lang: clojure
lead: jwass, edatta, dtkaczyk
legacy: false
owner: ppolischuk
packages: []
prod_heartbeats:
- https://api.crossref.org/heartbeat
prod_urls:
- https://api.crossref.org/v1/works
- https://api.crossref.org/v1/funders
- https://api.crossref.org/v1/members
- https://api.crossref.org/v1/prefixes
- https://api.crossref.org/v1/types
- https://api.crossref.org/v1/journals
- https://api.crossref.org/v1/licenses
products:
- metadata-retrieval
- metadata-search
- funder-registry
related_services: []
repo_links:
- https://gitlab.com/crossref/rest_api
sentry_url: ''
servicedeps:
- infra-datacenter-elastic
- infra-datacenter-docker
- infra-datacenter-mongo
- get-prefix-publisher
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://api.staging-legacy.crossref.org/v1/works
tags:
- clinical-trial-numbers
title: REST API (Cayenne)
userfacing: true
---




API for querying and retrieving data in JSON format. This serves most endpoints on api.crossref.org, but not all. i.e. 

- `api.crossref.org/works`
- `api.crossref.org/funders`
- `api.crossref.org/members`
- `api.crossref.org/prefixes`
- `api.crossref.org/types`
- `api.crossref.org/journals`
- `api.crossref.org/licenses`
