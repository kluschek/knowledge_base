---
related_services: []
repo_links: []
tags: []
title: REST API Design Notes
weight: 0
---



## Profiles

Profiles are a separate concept to Leiningen profiles. 

Known profile names:

- `:api` - Run the resource HTTP API.
- `:index` - Run an OAI download and index once daily.
- `:update-members` - Collect member records and update with metadata coverage stats (once a day).
- `:update-journals` - Collect journal records and update with metadata coverage stats (once a day).
- `:update-funders` - Load new funder registry RDF when available. Checks for new RDF once an hour.
- `:feed-api` - Must be specified along with :api. Enables the feed API for real-time metadata ingest.
- `:process-feed-files` - Run async processing of incoming feed files. Should be enabled with :feed-api.
