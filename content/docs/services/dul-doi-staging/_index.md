---
area: infrastructure-apps
components: ''
datadeps: []
desc: Testing endpoint for members testing integrations. Mocks out the doi.org handle
  server.
docs: []
lang: ''
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://dul_doi.staging-legacy.crossref.org/
products:
- distributed-usage-logging
related_services: []
repo_links:
- https://gitlab.com/crossref/dul_doi_mock
sentry_url: ''
servicedeps:
- ext-doi
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DUL DOI Staging Endpoint
userfacing: true
---

Testing endpoint for members testing integrations. Mocks out the doi.org handle server by retrieving real data, then patching in mocked configurable handle headers. Note that as this is designed exclusively for test as a stop-gap, the 'production' version is running on the `*.staging-legacy.crossref.org` subdomain.

## Mocking

Users wishing to test their integrations may want a 'mock' or staging version of the DOI server which behaves as the doi.org server, returning DUL headers.

The DUL DOI Mock tool behaves as a DOI server, returning custom headers. It currently runs at `dul_doi.staging-legacy.crossref.org`.

To demonstrate a DUL lookup call:

```
curl --verbose https://dul_doi.staging-legacy.crossref.org/10.5555/12345678
> GET /10.5555/12345678 HTTP/1.1
> Host: localhost:9438
> User-Agent: curl/7.49.1
> Accept: */*
>
< HTTP/1.1 200 OK
< Server: cloudflare
< Content-Type: text/html;charset=utf-8
< Content-Length: 0
< Connection: keep-alive
< Location: <http://example.com>; rel="dul"
< Expires: Fri, 24 Aug 2018 11:18:48 GMT
< Set-Cookie: __cfduid=ddbcccf7fb57189ecc736f0f6f8d8380a1535108311; expires=Sat, 24-Aug-19 10:58:31 GMT; path=/; domain=.doi.org; HttpOnly
< Cf-Ray: 44f54de0a7a96a67-LHR
< Date: Fri, 24 Aug 2018 10:58:31 GMT
< Vary: Accept
< Date: Fri, 24 Aug 2018 10:58:31 GMT
```

To contribute, the developer of a third party integration should supply a DOI prefix and URL endpoint pair, either by a pull request to the repository at <https://gitlab.com/crossref/dul_doi_mock/blob/master/resources/mapping.json> , or by contacting Crossref staff who can do this. Once merged it should then be re-deployed.
