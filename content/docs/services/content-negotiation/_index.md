---
area: distribution-querying
components: ''
datadeps:
- rest-api-cayenne
desc: Data and ID content negotiation proxy, backed by Cayenne API.
docs:
- https://support.crossref.org/hc/en-us/articles/213673586-Content-negotiation
lang: Clojure
lead: jwass
legacy: true
owner: ppolischuk
packages: []
prod_heartbeats: []
prod_urls:
- https://data.crossref.org/10.5555/12345678
products:
- metadata-retrieval
related_services: []
repo_links:
- https://github.com/CrossRef/cayenne-data
sentry_url: ''
servicedeps:
- rest-api-cayenne
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Content Negotiation
userfacing: true
---

Data and ID content negotiation proxy, backed by Cayenne API.
