---
area: infrastructure-services
components: ''
datadeps: []
desc: Docker Swarm running in AWS.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-aws
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Docker (AWS)
userfacing: false
---



Docker Swarm running in AWS.
