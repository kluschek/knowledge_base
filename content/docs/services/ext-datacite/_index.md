---
area: external
components: ''
datadeps:
- rest-api-cayenne
- event-data-query-api
desc: DataCite as a whole.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: DataCite
userfacing: false
---



DataCite as a whole.
