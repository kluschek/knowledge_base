---
area: registries
components: ''
datadeps:
- deposit-processor
desc: List of Clinical Trial Registries used to validate Clinical Trial links. Deposited
  as XML.
docs: []
lang: XML
lead: ''
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://doi.org/10.18810/registries
products:
- content-registration
related_services:
- crossref-schemas
repo_links: []
sentry_url: ''
servicedeps:
- openurl
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- clinical-trial-numbers
title: Clinical Trials Registries
userfacing: false
---




List of Clinical Trial Registries used to validate Clinical Trial links. 

This is deposited as DOI <https://doi.org/10.18810/registries>

Each Clinical Trial Registry is represented as a component DOI of this. Links in metadata to Clinical Trials should include a Registry DOI. See `clinicaltrials.xsd` in the Schema repository.

The [JATS4R recommendation for Clinical Trials](https://jats4r.org/clinical-trials) references this registry.
