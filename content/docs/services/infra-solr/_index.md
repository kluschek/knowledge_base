---
area: infrastructure-services
components: ''
datadeps: []
desc: Search Engine. Used by CS and REST API (deprecated).
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: SOLR
userfacing: false
---



Search Engine. Used by CS and REST API (deprecated).
