---
area: distribution-querying
components: ''
datadeps: []
desc: Execute various queries on Metadata via an API or the Admin system.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.QueryController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/servlet/query/**
- https://doi.crossref.org/query/xref.cgi/**
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- infra-datacenter-apache
- citation-search-service
- formatted-citation-parse
- ref-xpress-service
- authorization-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: XML Query System
userfacing: true
---



Execute various queries on Metadata via an API or the Admin system.
