---
area: documentation
components: meta
datadeps: []
desc: Wiki of our internal tools and services, including this diagram. Public, but
  for internal use only.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/knowledge_base
sentry_url: ''
servicedeps:
- infra-gitlab
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Internal Knowledge Base
userfacing: true
---



Wiki of our internal tools and services, including this diagram. Public, but for internal use only.
