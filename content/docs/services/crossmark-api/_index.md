---
area: distribution-querying
components: ''
datadeps: []
desc: Deprecated Crossmark API.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.CrossmarkQueryController
- org.crossref.qs.crossmark.CrossmarkDataFactory
- org.crossref.databasedaos.crossmark.CrossmarkRODaoImpl
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/crossmark/**
products:
- crossmark
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cs-artifact
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossmark API (Deprecated)
userfacing: false
---




Deprecated Crossmark service. Makes queries from `crossmark_relations` table. This has dependencies on some table updating code that can be removed.

