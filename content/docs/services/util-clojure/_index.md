---
area: tools-libraries
components: ''
datadeps: []
desc: Clojure library for common Crossref-related functions.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://github.com/CrossRef/util
sentry_url: ''
servicedeps:
- infra-clojars
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Util Library (Clojure)
userfacing: false
---



Clojure library for common Crossref-related functions.
