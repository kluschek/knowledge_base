---
area: infrastructure-apps
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.CreateNotificationController
- org.crossref.xs.notifications.NotificationServiceImpl
- org.crossref.xs.notifications.callback.NotificationCallbackNotifyEntityDaoImpl
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/notification/create
- https://doi.crossref.org/utility/notify/**
- https://doi.crossref.org/notification-callback/**
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- interim-storage
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Notification Service
userfacing: false
---




Content to follow.

