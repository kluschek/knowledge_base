---
area: infrastructure-services
components: ''
datadeps: []
desc: The Data Center-based servers that we operate.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Data Center
userfacing: false
---



The Data Center-based servers that we operate.
