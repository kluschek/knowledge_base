---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.bulkextractor3.BulkExtractorTool
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- members-service
- citation-reference-finder
- oai-pmh-dao
- book-cite-dao
- journal-cite-dao
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- snapshots
title: Bulk Extractor Tool
userfacing: true
---

Bulk Extractor Tool is used for producing Snapshots. See the [Metadata Snapshots]({{< relref "/docs/topics/metadata-snapshots" >}}).