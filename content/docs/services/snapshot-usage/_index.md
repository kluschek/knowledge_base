---
area: reporting-monitoring
components: ''
datadeps:
- rest-api-snapshots
desc: Track usage of XML and JSON Snapshot usage and serve reports.
docs: []
lang: ''
lead: jstark
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- metadata-plus
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- snapshots
title: Snapshot Usage
userfacing: false
---

Content to follow.

Track usage of XML and JSON Snapshot usage and serve reports.
