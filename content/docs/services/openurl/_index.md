---
area: distribution-querying
components: ''
datadeps:
- cddb
desc: OpenURL compliant API for retrieving XMl Metadata.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.OpenUrlController
prod_heartbeats: []
prod_urls:
- https://doi.org/openurl/**
products:
- metadata-retrieval
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- cddb
- citation-search-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: OpenURL
userfacing: true
---



OpenURL compliant API for retrieving XMl Metadata.
