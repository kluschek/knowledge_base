---
area: tools-libraries
components: ''
datadeps: []
desc: High-level Python library for consistently getting data from Crossref APIs (REST,
  XML, ...).
docs: []
lang: Python
lead: dtkaczyk
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/crossref_commons_py
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Crossref Commons (Python)
userfacing: false
---



High-level Python library for consistently getting data from Crossref APIs (REST, XML, ...).
