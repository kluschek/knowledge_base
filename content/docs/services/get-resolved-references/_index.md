---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.GetResolvedReferences
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/getResolvedRefs/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- authorization-service
- citation-reference-finder
- ext-doi
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Get Resolved References
userfacing: false
---



