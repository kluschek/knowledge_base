---
area: distribution-querying
components: ''
datadeps:
- rest-api-cayenne
- doi-citation-search
desc: Co-access DOI landing pages for books.
docs: []
lang: ''
lead: ''
legacy: true
owner: kmeddings
packages: []
prod_heartbeats: []
prod_urls:
- https://apps.crossref.org/search/coaccess
- https://apps.crossref.org/coaccess/coaccess.html
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- infra-datacenter-apache
- doi-citation-search
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- authenticated
title: Co-access pages
userfacing: true
---




Co-access DOI landing pages for books.

DOI redirects to the HTML page, e.g. 

 - <https://apps.crossref.org/coaccess/coaccess.html?doi=10.2307%2Fj.ctt7zvwrk>
 
this then makes a retrieval to the API from the browser:

 - <https://apps.crossref.org/search/coaccess?doi=10.2307%2Fj.ctt7zvwrk>

HTML page calls the API from the client.

## Parameters

`doi` must be supplied.

## Notes

Has an implicit dependency on DOI Citation Search via an HTTP call using password supplied by properties in the Spring Context bean.


