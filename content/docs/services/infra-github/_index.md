---
area: infrastructure-services
components: ''
datadeps: []
desc: GitHub as a platform for serving files, issue-tracking, etc.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://www.github.com
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: GitHub (platform)
userfacing: false
---



GitHub as a platform for serving files, issue-tracking, etc.
