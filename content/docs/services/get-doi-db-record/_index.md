---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.xs.doidbrecord.GetDoiDBrecordHttpController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/search/getdoidbrecord
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Get DOI Record
userfacing: false
---




Content to follow.

