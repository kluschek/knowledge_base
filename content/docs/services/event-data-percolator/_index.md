---
area: etl
components: ''
datadeps:
- ext-doi
- event-data-artifact-registry
desc: Service to identify, filter and extract Events from inputs fed to it by Agents
  as part of Crossref Event Data.
docs: []
lang: ''
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://gitlab.com/crossref/event_data_percolator
sentry_url: ''
servicedeps:
- infra-hetzner-kafka
- infra-hetzner-docker
- event-data-docker-base
- util-clojure
- infra-hetzner-elk
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Percolator
userfacing: false
---



Service to identify, filter and extract Events from inputs fed to it by Agents as part of Crossref Event Data.
