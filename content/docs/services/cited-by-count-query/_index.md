---
area: distribution-querying
components: ''
datadeps: []
desc: Distribute internal updates about cited-by counts per DOI.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- cited-by
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: sentry.io/organizations/crossref/issues/?project=2095999
staging_heartbeats: []
staging_urls: []
tags: []
title: Cited-by Count Query System
userfacing: false
---



Distribute internal updates about cited-by counts per DOI.
