---
area: distribution-querying
components: ''
datadeps:
- members-service
desc: Get list of prefixes and publisher members
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.GetPublisherController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/getPrefixPublisher/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- members-service
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- member-info
- authenticated
title: Get Prefix Publisher
userfacing: false
---




This exposes two APIs. Both require authentication tokens.

### All members

    http://doi.crossref.org/getPrefixPublisher/?prefix=all

This is returned in JSON format.

### Per owner prefix

    http://doi.crossref.org/getPrefixPublisher/?prefix=«PREFIX»

This is returned in XML format.