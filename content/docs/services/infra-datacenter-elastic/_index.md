---
area: infrastructure-services
components: ''
datadeps: []
desc: Search engine.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Elastic Search (Datacenter)
userfacing: false
---



Search engine.
