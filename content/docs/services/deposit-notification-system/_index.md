---
area: infrastructure-apps
components: ''
datadeps:
- deposit-processor
desc: Register for emails, notifications or callbacks for events within the Deposit
  Processor and other parts of the system.
docs: []
lang: Java
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls: []
products:
- content-registration
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps:
- interim-storage
- infra-datacenter-tomcat
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Deposit Notification System
userfacing: true
---



Register for emails, notifications or callbacks for events within the Deposit Processor and other parts of the system.

Notifications do not have custom HTTP headers.
