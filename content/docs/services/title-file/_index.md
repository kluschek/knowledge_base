---
area: registries
components: ''
datadeps: []
desc: ''
docs: []
lang: CSV
lead: ''
legacy: true
owner: ''
packages: []
prod_heartbeats: []
prod_urls:
- https://ftp.crossref.org/titlelist/titleFile.csv
products:
- metadata-manager
related_services: []
repo_links: []
sentry_url: ''
servicedeps:
- infra-ftp
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- journal-titles
title: Titles File
userfacing: false
---




Note that the `ftp.crossref.org` URL is not available inside the data center. Use `vftp.crossref.org` when running in the data center or VPN. 
