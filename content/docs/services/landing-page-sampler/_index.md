---
area: collection
components: ''
datadeps:
- event-data-artifact-registry
- rest-api-cayenne
desc: Sample our landing pages for use by Agents. Creates Artifacts.
docs: []
lang: Clojure
lead: jwass
legacy: true
owner: bvickery
packages: []
prod_heartbeats: []
prod_urls: []
products:
- event-data
related_services: []
repo_links:
- https://github.com/CrossRef/event-data-landing-page-sampler
sentry_url: ''
servicedeps:
- event-data-artifact-registry
- infra-aws-docker
- infra-s3
- util-clojure
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Event Data Landing Page Sampler
userfacing: false
---



Sample our landing pages for use by Agents. Creates Artifacts.
