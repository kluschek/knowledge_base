---
related_services: []
repo_links: []
tags: []
title: CrossMark Dialog
weight: 0
---



## How

The application is hosted at https://crossmark.crossref.org and is mirrored, in its entirety, in a CloudFront distribution at https://crossmark-cdn.crossref.org . The CDN is only intended for retrieval of assets (CSS, image and JavaScript files). While it could be used to proxy the dialog box, this is not the indended use. This simple set-up was chosen over alernatives e.g. a dedicated CDN for asset files, was chosen for three reasons. 

## Why use a CDN

1. It avoids extra build pipeline stages during deployment. There is no step to 'copy assets to CDN'.
2. It simplifies development and staging, as all of the CDN-dependent parts can simply point to the staging or development instance without further compilation.
3. The server generates the JavaScript widget code dynamically, including a time-based token that allows the script to self-report on its own version. This is included to help us detect when users (i.e. publishers who implement the dialog on their site) implement per instructions, or when they ignore the instructions. See (see [Crossmark JWTs](/crossmark/jwt))

## Benefits

The CDN provides multiple benefits:

 - reliable hosting for mission-critical assets that must work
 - speedy serving of assets to users via geo-local edge servers
 - ability to roll out new versions for bugfixes without co-ordinating with all implementing members


