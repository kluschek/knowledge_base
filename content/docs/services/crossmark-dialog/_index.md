---
area: distribution-querying
components: ''
datadeps:
- rest-api-cayenne
desc: Dialog and widget code for inclusion in members' sites.
docs:
- https://www.crossref.org/get-started/crossmark/
lang: Clojure
lead: jwass
legacy: true
owner: kmeddings
packages: []
prod_heartbeats:
- https://crossmark.crossref.org/heartbeat
prod_urls:
- https://crossmark.crossref.org
products:
- crossmark
related_services: []
repo_links:
- https://github.com/CrossRef/crossmark
sentry_url: ''
servicedeps:
- rest-api-cayenne
- crossmark-cdn
- infra-datacenter-mysql
sonar_url: ''
staging_heartbeats: []
staging_urls:
- https://crossmark.staging-legacy.crossref.org
tags: []
title: CrossMark Dialog
userfacing: true
---




Crossmark gives readers quick and easy access to the current status of an item of content. With one click, you can see if content has been updated, corrected or retracted and access valuable additional metadata provided by the publisher.