---
area: distribution-querying
components: ''
datadeps: []
desc: ''
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.qs.controllers.FuzzySearchController
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org/search/fuzzy-title-score/**
products: []
related_services: []
repo_links: []
sentry_url: ''
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags:
- matching
title: Fuzzy Title Score
userfacing: false
---




Content to follow.

