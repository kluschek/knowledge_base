---
area: infrastructure-apps
components: ''
datadeps: []
desc: Find member information. Will be deprecated.
docs: []
lang: ''
lead: ''
legacy: true
owner: ''
packages:
- org.crossref.xs.members.MembersService
prod_heartbeats: []
prod_urls:
- https://doi.crossref.org
products: []
related_services: []
repo_links:
- https://gitlab.com/crossref/content_system
sentry_url: https://sentry.io/organizations/crossref/issues/?project=1769602
servicedeps: []
sonar_url: ''
staging_heartbeats: []
staging_urls: []
tags: []
title: Members Service
userfacing: false
---




Content to follow.

