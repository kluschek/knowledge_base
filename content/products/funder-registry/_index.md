---
docs: []
owner: ''
prod_urls: []
title: Funder Registry
weight: 0
---

The Funder Registry allows everyone to have transparency into research funding and its outcomes. It’s an open and unique registry of persistent identifiers for grant-giving organizations around the world.