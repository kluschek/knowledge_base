---
docs: []
owner: ''
prod_urls: []
title: Similarity Check
weight: 0
---

A service provided by Crossref and powered by iThenticate—Similarity Check provides editors with a user-friendly tool to help detect plagiarism.