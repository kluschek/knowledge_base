---
docs: []
owner: ''
prod_urls: []
title: Crossmark
weight: 0
---

The Crossmark button lets readers see the status of a work by displaying any corrections, retractions, or updates to that record.
