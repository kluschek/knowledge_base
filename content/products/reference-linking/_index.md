---
docs: []
owner: ''
prod_urls: []
title: Reference Linking
weight: 0
---

Reference Linking enables researchers to follow a link from the reference list to other full-text documents, helping them to make connections and discover new things. 