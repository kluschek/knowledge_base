---
docs: https://support.crossref.org/hc/en-us/articles/214169586
owner: ppolischuk
prod_urls:
- https://www.crossref.org/services/content-registration/
title: Content Registration
weight: 0
---

## Description

Members register content with us by sending metadata about their published content. Metadata is generally in the form of XML and can be deposited manually (e.g. through our Metadata Manager tool) or through machine use (e.g. HTTPS POST). A family of systems support this core functionality.

## Status story

The content registration system is typically updated on a weekly release schedule. See the #operations channel on Tuesday mornings for more information.




