---
doc_link:
- https://docs.google.com/document/d/11LYFxJjhT-tZ839rbLSNEqWmA2TDaqQgZqipHR8WE1M
label_name: C::Crossmark and Page Enhancements
title: DOI / Crossmark Page Enhancements
weight: 0
---

JavaScript Plug-ins for DOI landing pages. Currently the Crossmark widget, which may be expanded.

