---
doc_link:
- https://docs.google.com/document/d/1QgT-zkmSB90YrUlNLteHUK-7rwkAim8LezGBy9rzNNc/edit
label_name: null
title: Landing Pages
weight: 0
---
Serve up landing pages for DOIs, e.g. co-access, deleted DOIs, pending publications, etc. Provide easy access for relevant parties to be able to edit them (internal support or members directly).
