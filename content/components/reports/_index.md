---
doc_link:
- https://docs.google.com/document/d/1IJwcVi5ilVNMqJdEh6o89YMdzWGjZbU8w5fQgIjpu3Y/
label_name: C::Reports
title: Reports
weight: 0
---

A set of documents (data and/or HTML) that report on various things. With data drawn from the Activity Log, Query APIs and other well-known places, they can be well-defined and clearly specced. It will also be easier to add new ones. By placing them all in the same place, and allowing uniform access, they can be much more usable and useful. We can instrument access to measure how useful they are. 

