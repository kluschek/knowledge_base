---
doc_link:
- https://docs.google.com/document/d/1tYHKIeqeP5oWisIa2IyFIcNqilwRvDkqKQ9joTGvsKM/edit
label_name: C::Activity Log and Metrics
title: Activity Log and Metrics
weight: 0
---

Central Log of all activity that happens in Crossref, plus durable metrics for querying behaviour over time. 
 
Every Deposit, title match, ingestion, alternation to the XML, deposit. Structured and searchable, this forms both an audit log for generating billing reports, an indication of activity in the system, a source of notifications, and a source of interim reporting (such as 'how many deposits did I make this month?').
