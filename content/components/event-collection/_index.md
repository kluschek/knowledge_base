---
doc_link:
- https://docs.google.com/document/d/1R24h9ul4pD64WMy8FlKoVYRzg16CXlxgTCGgOLU0BzM/edit
label_name: null
title: Event Collection
weight: 0
---

Collect and process Events. Agents connect to external sources to collect data, extract events and send into the Event bus.

