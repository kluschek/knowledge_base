---
doc_link:
- https://docs.google.com/document/d/1vAdVNpqi2gcQJMvyibLQtV71EcMRmt5WTmxAmO-JdfI/edit
label_name: C::Community Data and Auth
title: Community Data & Authentication
weight: 0
---

Represent our Membership and Community structures, who they are, and what they can do. Perform Access Control, billing, self-service and many other functions.  Store and Authenticate Member Credentials, password reset flow.

This will ultimately represent things like:

 - database of users, members, prefixes and how they relate to each other
 - list of domain names per member
 - list of sponsoring organisations, and which accounts they can administrate
 - mapping to external Intacct IDs and Sugar IDs
 - list of capabilities (who can deposit, query etc which DOIs)
 - list of other per-member metadata, like DUL endpoints
 - list of service providers and other integrations

Provides the canonical model of our membership and the central Access Control API.
