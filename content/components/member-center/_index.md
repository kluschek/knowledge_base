---
doc_link:
- https://docs.google.com/document/d/1ebgKZ_zuiWtzuX1sVNrfQIAwTQNiEevi11i9_w_Vf2M/edit
label_name: C::Member Center
title: Member Center
weight: 0
---

Enable Members and Users to administer their accounts, run queries, retrieve reports. Notifications can be administered (either via UI or API). 

Features may include API and User Interface for:

 - Administration by internal staff
 - Initial sign-up for members
 - Member self-service tokens
 - Members can add and remove users
 - Members can administer their own account & information.
 - Retrieve reports
 - Retrieve submissions, queue status and related events
 - Submit bulk patches to metadata
 - Update individual metadata records
 - Accept assertions
 - Manage notifications

