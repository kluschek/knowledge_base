---
doc_link:
- https://docs.google.com/document/d/1uqv3-_rDDZaRvfNvZ-yIgYyS1iU0Ot2OXaqM3BUK3Zk/edit
label_name: null
title: Event Data Evidence Log & Snapshots
weight: 0
---

Evidence Log of all activity happening in Event Data, plus regular public snapshots.
