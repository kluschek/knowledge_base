---
doc_link: []
label_name: null
title: Funder Ingestion
weight: 0
---
Ingest Funder Registry via an API, with UI layered on top. Update the registry, registering new DOIs if needed.

