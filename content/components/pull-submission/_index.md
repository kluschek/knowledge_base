---
doc_link:
- https://docs.google.com/document/d/1grKa-zouKXUwKhY_M3iR_-jKeLV6y_rpW-IxHlX8XI4
label_name: null
title: Pull-Submission
weight: 0
---

Allow members to send us deposits by new means, e.g. pushing to an S3 bucket that we pull from.

