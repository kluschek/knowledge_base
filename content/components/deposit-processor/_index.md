---
doc_link:
- https://docs.google.com/document/d/1O_BW_EpIZLzz-MOSn8EV9Hlu4K9mgodM8deBjeCSqNQ/edit
label_name: null
title: Deposit Processor
weight: 0
---

Accept Deposits / Submissions and incorporate them into the Metadata Repository. Validate against the schema, run checks.

