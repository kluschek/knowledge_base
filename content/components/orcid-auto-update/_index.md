---
doc_link: []
label_name: null
title: ORCID Auto Update
weight: 0
---

Send authorship assertions made by funders to ORCID users’ inboxes.

