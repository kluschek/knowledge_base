---
doc_link: []
label_name: null
title: Conflict Detection
weight: 0
---

Detect conflicting deposits and raise notifications or reject.

