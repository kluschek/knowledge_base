---
doc_link:
- https://docs.google.com/document/d/1EXvrO-dPXcKdIMRU9hUVhf7ahmNbPnrnVcVzwkOXwhc/edit
label_name: null
title: Event Data Artifacts
weight: 0
---
Registry of Artifacts (data documents) that control the behaviour of Event Data Agents.
