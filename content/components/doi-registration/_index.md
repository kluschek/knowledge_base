---
doc_link:
- https://docs.google.com/document/d/1vhVGX-I9ABGcOodYfEw0rYIbB-ichkIJ-hJFIJbgQow/edit
label_name: null
title: DOI Registration
weight: 0
---

Registers DOI metadata with the Handle system. 
