---
doc_link:
- https://docs.google.com/document/d/1G0YBa1nqr4-ZwQQnZev5Ew8XM4KB1jdhDvCq5mrWbO0/edit
label_name: C::Metadata Repository
title: Metadata Repository
weight: 0
---

A store of XML metadata. Optionally with JSON representations alongside. An internal store and API that allows the Deposit Processor to register new types, plus the Metadata Enhancement to plug in.
