---
doc_link:
- https://docs.google.com/document/d/1QlJ9RIbfURVY37jYuAlYjASupkisYOcbJdIX7or8_sw/edit#
label_name: null
title: Snapshots
weight: 0
---

Compressed files that contain a representation of our metadata, both in UNIXSD XML and REST API JSON format. Created on a periodic basis for our Plus service.


