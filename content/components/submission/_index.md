---
doc_link:
- https://docs.google.com/document/d/1kk52Szj63eXXfrQ19LI0s5NqyLT3DCC_0mlO-vFkuMs
label_name: C::Submission
title: Deposit (Submission) API & Queue
weight: 0
---

Receive submissions from members. Submit them to the Deposit Processor. Manage a queue, access control. UI accessible through Member Center.
