---
doc_link: []
label_name: null
title: Bulk Book Title Update
weight: 0
---

Content System project package for updating book titles using a CSV file as input.

The column headings allowed are:
- new title (any instance of new_title, new-title, and any uppercase variant, convert)
- pisbn (p isbn, p-isbn, p_isbn, isbn, and any uppercase variant convert)
- eisbn (e isbn, e-isbn, e_isbn, and any uppercase variant convert)
- bookciteid (any uppercase variant converts)
- doi (any uppercase variant converts)
- old title (any instance of old_title, old-title, current title, current_title, current-title, and any uppercase variant, convert)

Required columns:
- new title is ALWAYS required
- at least one of: bookciteid, doi, or at least one isbn (isbn, pisbn, eisbn)

Precedence:
- If more than one type of identifier (bookciteid, doi, isbn) is provided, they are attempted in a preferred order.
  1. bookciteid
  2. doi
  3. isbn(s)

   Once the title has found and updated a title, it will stop trying the next identifier.
 
 If a solid match is not made, no update is made and the skipped entry is noted in the log.
 If the existing title matches the new title, no update is done and it is noted in the log.
 
 