---
doc_link:
- https://docs.google.com/document/d/1WZvE-KK_IPlObJkLewlq4Ao1R8mzH5UxwVGel25fr_g
label_name: null
title: Metadata Enhancement Interface
weight: 0
---

Provide a single interface through which components can make changes to the metadata. For example, matching and references. This allows us to plug in different implementations over time. Changes are represented on the Activity Log, so that other components can observe behaviour.
