---
doc_link:
- https://docs.google.com/document/d/1MIga26xoQq63uLA8ERA9psX3v8yP3L6TOMEzwKXWbxk/edit#
label_name: null
title: Greenfield Components
weight: 0
---

The internal [Software Development Plan](https://docs.google.com/document/d/1MIga26xoQq63uLA8ERA9psX3v8yP3L6TOMEzwKXWbxk/edit#) document gives more context.

Components describe the functional areas that the Crossref's [Greenfield]({{< relref "/docs/development/quality-defects" >}}) architecture. Some components will correspond directly to a service with a single codebase, but some will be implemented by a cluster of services.

![Long Term Architecture Diagram](long-term-architecture.png)
[Full size image](long-term-architecture.png)

Because this is designated for only new Greenfield code, and we're at the start of that journey, most of these will not have any services associated with them yet. 

This list will evolve over time as we analyse our product and technology requirements.