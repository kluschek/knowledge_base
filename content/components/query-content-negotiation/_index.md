---
doc_link:
- https://docs.google.com/document/d/1rg_ZNet_CwyMfov7z3ggnbT-tSWUd90ukDx1CMpyX6E/edit
label_name: C::Query and Content Negotiation
title: Query & Content Negotiation
weight: 0
---

Allow the community to issue queries against our metadata. Our current set of queries include:

 - Cayenne REST API
 - XML live query system
 - XML stored query system
 - Event Data Query API
 - OAI-PMH
 - Custom TurnItIn feed

Whether we try to build something feature-compatible with our XML query APIs, or deprecate them in favour of the REST API and new Notifications solution, is an open question. Our Cayenne REST API can be modified to return XML from its results and to include Event Data events.
