---
doc_link: []
label_name: null
title: Forward Link Events
weight: 0
---
Monitor new references to registered content and alert those interested in knowing about those new references.

