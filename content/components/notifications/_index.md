---
doc_link: []
label_name: null
title: Notifications
weight: 0
---

Background system to send notifications to users, either as pingbacks or emails. Monitors the Activity Log and matches things that happen according to various rules. These can be configurable via an API that can be automated by the Member Center. 

